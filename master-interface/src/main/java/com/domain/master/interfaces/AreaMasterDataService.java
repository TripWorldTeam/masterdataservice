/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.AreaM;

/**
 *
 * @author root1
 */
public interface AreaMasterDataService {

    /**
     * Get All Areas
     *
     * @return *
     * @throws com.domain.master.interfaces.AppException
     */
    List<AreaM> getAllArea() throws AppException;

    /**
     * Get Area Record by Name
     *
     * @param areaName
     * @return
     * @throws com.domain.master.interfaces.AppException
     */
    AreaM getAreaName(String areaName) throws AppException;

    
    /**
     * Get Area Record by ID
     *
     * @param areaId
     * @return
     * @throws com.domain.master.interfaces.AppException *
     */
    AreaM getAreaId(String areaId) throws AppException;

    
}
