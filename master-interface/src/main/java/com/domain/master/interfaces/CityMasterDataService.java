/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.CountryTO;
import com.domain.master.masterservice.dto.City;

/**
 *
 * @author root1
 */
public interface CityMasterDataService {

    /**
     * Get All Cities
     *
     * @return *
     * @throws com.domain.master.interfaces.AppException
     */
    List<City> getAllCities() throws AppException;

    /**
     * Get City Record by Name
     *
     * @param cityName
     * @return
     * @throws com.domain.master.interfaces.AppException
     */
    City getCityName(String cityName) throws AppException;

    /**
     * Get City Record by Name Like
     *
     * @param cityName
     * @return  *
     * @throws com.domain.master.interfaces.AppException
     */
    List<City> getCityNameLike(String cityName) throws AppException;

    /**
     * Get City Record by ID
     *
     * @param cityId
     * @return
     * @throws com.domain.master.interfaces.AppException *
     */
    City getCityId(Long cityId) throws AppException;

    /**
     * Get City Record by Country ID
     *
     * @param countryId
     * @return  *
     * @throws com.domain.master.interfaces.AppException
     */
    City getCitiesByCountryId(Long countryId) throws AppException;

}
