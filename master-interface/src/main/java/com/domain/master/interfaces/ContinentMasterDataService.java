/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.Continent;

/**
 *
 * @author root1
 */
public interface ContinentMasterDataService {

    /**
     * Get All Continents
     *
     * @return *
     * @throws com.domain.master.interfaces.AppException
     */
    List<Continent> getAllContinents() throws AppException;

    /**
     * Get Content Record by Name
     *
     * @param contentName
     * @return
     * @throws com.domain.master.interfaces.AppException
     */
    Continent getContinentName(String contentName) throws AppException;

    
    /**
     * Get Continent Record by ID
     *
     * @param contentId
     * @return
     * @throws com.domain.master.interfaces.AppException *
     */
    Continent getContinentId(String contentId) throws AppException;

    
    
}
