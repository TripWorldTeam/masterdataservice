/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.Currency;

/**
 *
 * @author root1
 */
public interface CurrencyMasterDataService {

    /**
     * Get All Areas
     *
     * @return *
     * @throws com.domain.master.interfaces.AppException
     */
    List<Currency> getAllCurrencies() throws AppException;

    /**
     * Get Area Record by Name
     *
     * @param currencyCode
     * @return
     * @throws com.domain.master.interfaces.AppException
     */
    Currency getCurrencyCode(String currCode) throws AppException;

    

    
}
