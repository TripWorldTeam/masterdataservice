/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.Customers;

/**
 *
 * @author root1
 */
public interface CustomerMasterDataService {

    /**
     * @return 
     * @throws com.domain.master.interfaces.AppException
     */
    List<Customers> getAllCustomers() throws AppException;
    /**
     * @param customer
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    List<Customers> getCustomerNameLike(String customer) throws AppException;
    /**
     * @param cusName
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    Customers getCustomerName(String cusName) throws AppException;
    /**
     * @param cusId
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    Customers getCustomerId(String cusId) throws AppException;


    
}
