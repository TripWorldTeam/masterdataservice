package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.CustomerTier;
import com.domain.master.masterservice.dto.Customers;
import com.domain.master.masterservice.dto.Continent;

public interface CustomerTierMasterDataService {

    List<CustomerTier> getAllCustomerTier() throws AppException;

    /**
     * Get Tier Record by Name
     *
     * @param tierName
     * @return
     * @throws com.domain.master.interfaces.AppException
     */
    CustomerTier getCustomerTierName(String tierName) throws AppException;

    /**
     * Get Tier Record by Name Like
     *
     * @param tierName
     * @return  *
     * @throws com.domain.master.interfaces.AppException
     */
    List<CustomerTier> getCustomerTierNameLike(String tierName) throws AppException;

    /**
     * Get Customer Tier Record by ID
     *
     * @param customerId
     * @return
     * @throws com.domain.master.interfaces.AppException *
     */
    CustomerTier getCustomerId(String customerId) throws AppException;

    /**
     * Get tierId Record by ID
     *
     * @param tierId
     * @return
     * @throws com.domain.master.interfaces.AppException *
     */
    CustomerTier getCustomerTierId(Long tierId) throws AppException;


   
    

}
