package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.CustomerType;

public interface CustomerTypeMasterDataService {

    List<CustomerType> getAllCustomerType() throws AppException;

    
    /**
     * Get Type Record by Name Like
     *
     * @param cTypeDesc
     * @return  *
     * @throws com.domain.master.interfaces.AppException
     */
    List<CustomerType> getCustTypeDescLike(String cTypeDesc) throws AppException;

    
    /**
     * Get tierId Record by ID
     *
     * @param typeId
     * @return
     * @throws com.domain.master.interfaces.AppException *
     */
    CustomerType getCustTypeId(Long typeId) throws AppException;

   

   
    

}
