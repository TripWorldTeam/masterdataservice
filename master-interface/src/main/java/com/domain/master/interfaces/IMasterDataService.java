package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.CountryTO;
import com.domain.master.masterservice.dto.City;
import com.domain.master.masterservice.dto.AreaM;
import com.domain.master.masterservice.dto.Customers;
import com.domain.master.masterservice.dto.Continent;

public interface IMasterDataService {

    List<CountryTO> getAllCountries();

    CountryTO getCountry(String countryCode);
    
    

   
    

}
