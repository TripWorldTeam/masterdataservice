/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.CountryTO;
import com.domain.master.masterservice.dto.Landmark;

/**
 *
 * @author root1
 */
public interface LandmarkMasterDataService {

    /**
     * Get All Landmark
     *
     * @return *
     * @throws com.domain.master.interfaces.AppException
     */
    List<Landmark> getAllLandmark() throws AppException;

    /**
     * Get Landmark Record by ID
     *
     * @param landId
     * @return
     * @throws com.domain.master.interfaces.AppException *
     */
//    Landmark getLandmarkId(String landId) throws AppException;
//
//    /**
//     * Get Landmark Record by Name
//     *
//     * @param cityName
//     * @return
//     * @throws com.domain.master.interfaces.AppException
//     */
//    Landmark getLandmarkDescription(String landmarkDescription) throws AppException;
//
//    /**
//     * Get Landmark Record by Name Like
//     *
//     * @param cityName
//     * @return  *
//     * @throws com.domain.master.interfaces.AppException
//     */
//    List<Landmark> getLandmarkTypeId(String cityName) throws AppException;
//
//    
//    /**
//     * Get Landmark Record by Country ID
//     *
//     * @param countryId
//     * @return  *
//     * @throws com.domain.master.interfaces.AppException
//     */
//    Landmark getLandmarkByCountryId(Long countryId) throws AppException;

}
