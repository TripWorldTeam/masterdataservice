package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.LandmarkType;

public interface LandmarkTypeMasterDataService {

    List<LandmarkType> getAllLandmarkType() throws AppException;

    
    /**
     * Get Type Record by Name Like
     *
     * @param TypeDesc
     * @return  *
     * @throws com.domain.master.interfaces.AppException
     */
    List<LandmarkType> getLandmarkDescriptionLike(String TypeDesc) throws AppException;

    
    /**
     * Get tierId Record by ID
     *
     * @param typeId
     * @return
     * @throws com.domain.master.interfaces.AppException *
     */
    LandmarkType getLandmarkTypeId(String typeId) throws AppException;

   

   
    

}
