/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.Partner;

/**
 *
 * @author root1
 */
public interface PartnerMasterDataService {

    /**
     * @return 
     * @throws com.domain.master.interfaces.AppException
     */
    List<Partner> getAllPartner() throws AppException;
    /**
     * @param partner
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    List<Partner> getPartnerNameLike(String partner) throws AppException;
    /**
     * @param cusName
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    Partner getPartnerName(String cusName) throws AppException;
    /**
     * @param cusId
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    Partner getPartnerId(String cusId) throws AppException;


    
}
