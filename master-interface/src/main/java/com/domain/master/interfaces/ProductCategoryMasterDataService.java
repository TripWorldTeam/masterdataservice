/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.ProductCategory;

/**
 *
 * @author root1
 */
public interface ProductCategoryMasterDataService {

    /**
     * @return 
     * @throws com.domain.master.interfaces.AppException
     */
    List<ProductCategory> getAllProductCategory() throws AppException;
    /**
     * @param cusId
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    ProductCategory getProductCategoryId(String cusId) throws AppException;


    
}
