/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.Product;

/**
 *
 * @author root1
 */
public interface ProductMasterDataService {

    /**
     * @return 
     * @throws com.domain.master.interfaces.AppException
     */
    List<Product> getAllProduct() throws AppException;
    /**
     * @param partner
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    List<Product> getProductNameLike(String partner) throws AppException;
    /**
     * @param cusName
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    Product getProductName(String cusName) throws AppException;
    /**
     * @param cusId
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    Product getProductId(String cusId) throws AppException;


    /**
     * @param partner
     * @return 
     * @throws com.domain.master.interfaces.AppException*/
    List<Product> getPartnerId(String partner) throws AppException;
    
}
