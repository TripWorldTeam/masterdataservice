package com.domain.master.interfaces;

import java.util.List;

import com.domain.master.masterservice.dto.ProductType;

public interface ProductTypeMasterDataService {

    List<ProductType> getAllProductType() throws AppException;

    
    /**
     * Get Type Record by Name Like
     *
     * @param cTypeDesc
     * @return  *
     * @throws com.domain.master.interfaces.AppException
     */
    List<ProductType> getProductTypeDescriptionLike(String cTypeDesc) throws AppException;

    
    /**
     * Get tierId Record by ID
     *
     * @param typeId
     * @return
     * @throws com.domain.master.interfaces.AppException *
     */
    ProductType getProductTypeId(Long typeId) throws AppException;

   

   
    

}
