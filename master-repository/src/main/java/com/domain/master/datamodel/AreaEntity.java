/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.datamodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Query;
import javax.persistence.NamedQuery;


@Entity
@Table(name = "area_master")

/**
 *
 * @author root1 setting up the Area table Entities
 */
public class AreaEntity extends BaseEntity {

    @Column(name = "areaid")
    @Id
    private String areaId;

    @Column(name = "areaname")
    private String areaName;
   
    @Column(name = "citycode")
    private String cityCode;

    /*get the City Id*/
    public String getId() {
        return areaId;
    }

    /*get the City Id*/
    public void setId(String id) {
        this.areaId = id;
    }

    /*get the Area Id*/
    public String getAreaId() {
        return areaId;
    }

    /*set the Country Id*/
    public void setAreaId(String AreaID) {
        this.areaId = AreaID;
    }

    /*get the AreaName*/
    public String getAreaName() {
        return areaName;
    }

    /*set the AreaNames*/
    public void setAreaName(String name) {
        this.areaName = name;
    }

    /*get the City Code*/
    public String getCityCode() {
        return cityCode;
    }

    /*set the City Name*/
    public void setCityCode(String code) {
        this.cityCode = code;
    }
    
}
