/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.datamodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Query;
import javax.persistence.NamedQuery;


@Entity
@Table(name = "crs_cities")

/**
 *
 * @author root1 setting up the City table Entities
 */
public class CityEntity extends BaseEntity {

    @Column(name = "city_id")
    @Id
    private Long cityId;

    @Column(name = "country_id")
    private Long countryId;

    @Column(name = "city")
    private String city;
    /*Complete City Address with location*/

    @Column(name = "country_name")
    private String countryName;

    @Column(name = "city_name")
    private String cityName;

    /*get the City Id*/
    public Long getId() {
        return cityId;
    }

    /*get the City Id*/
    public void setId(Long id) {
        this.cityId = id;
    }

    /*get the Country Id*/
    public Long getCountryId() {
        return countryId;
    }

    /*set the Country Id*/
    public void setCountryId(Long country_id) {
        this.countryId = country_id;
    }

    /*get the Complete City Address*/
    public String getCity() {
        return city;
    }

    /*set the Complete City Address*/
    public void setCity(String city) {
        this.city = city;
    }

    /*get the City Name*/
    public String getCityName() {
        return cityName;
    }

    /*set the City Name*/
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /*get the Country Name*/
    public String getCountryName() {
        return countryName;
    }

    /*set the City Name*/
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

}
