/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.datamodel;

import java.util.Date;
import javafx.scene.text.Text;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Query;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;


@Entity
@Table(name = "continent_master")

/**
 *
 * @author root1 setting up the City table Entities
 */
public class ContinentEntity extends BaseEntity {

    @Column(name = "continentid")
    @Id
    private int continentId;

    @Column(name = "continentname")
    private String continentName;

    @Column(name = "countrycode")
    private String countryCode;

    
    

   /*get the Customer Id*/
    public Integer getId() {
        return continentId;
    }

    /*get the Customer Id*/
    public void setId(Integer id) {
        this.continentId = id;
    }
    public Integer getContinentId() {
        return continentId;
    }

    /*get the Customer Id*/
    public void setContinentId(Integer id) {
        this.continentId = id;
    }

    /*get the Customer Name*/
    public String getcontinentName() {
        return continentName;
    }

    /*set the Customer Name */
    public void setcontinentName(String cusName) {
        this.continentName = cusName;
    }

    /*get the Complete City Address*/
    public String getCountryCode() {
        return countryCode;
    }

    /*set the Complete City Address*/
    public void setCountryCode(String country_code) {
        this.countryCode = country_code;
    }
    

}
