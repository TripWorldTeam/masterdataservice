/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.datamodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Query;
import javax.persistence.NamedQuery;

@Entity
@Table(name = "currency_master")

/**
 *
 * @author root1 setting up the City table Entities
 */
public class CurrencyEntity extends BaseEntity {

    @Column(name = "currencycode")
    @Id
    private String currencyCode;

    @Column(name = "currencydescription")
    private String currencyDescription;
    

   
    /*get the Currency Id*/
    public String getId() {
        return currencyCode;
    }

    /*get the CurrencyCode Id*/
    public void setId(String id) {
        this.currencyCode = id;
    }

    /*get the Currency Id*/
    public String getCurrencyCode() {
        return currencyCode;
    }

    /*set the Currency Id*/
    public void setCurrencyCode(String currCode) {
        this.currencyCode = currCode;
    }

    /*get the CurrencyDescription*/
    public String getCurrencyDescription() {
        return currencyDescription;
    }

    /*set the CurrencyDescription*/
    public void setCurrencyDescription(String description) {
        this.currencyDescription = description;
    }

    

}
