/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.datamodel;

import java.util.Date;
import javafx.scene.text.Text;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Query;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;


@Entity
@Table(name = "customer")

/**
 *
 * @author root1 setting up the City table Entities
 */
public class CustomerEntity extends BaseEntity {

    @Column(name = "customerid")
    @Id
    private String customerId;

    @Column(name = "customername")
    private String customerName;

    @Column(name = "customeraddress")
    private String customerAddress;

    @Column(name = "customercontactemail")
    private String customerContactEmail;

    @Column(name = "customercontactphone")
    private String customerContactPhone;
    
    @Column(name = "contactcontactsocial")
    private String contactContactSocial;
    
    @Column(name = "customerlocationlatitude")
    private Double customerLocationLatitude;
    
    @Column(name = "linkedcustomer")
    private String linkedCustomer;
    
    @Column(name = "customervalue")
    private Integer customerValue;

    
    @Column(name = "registereddate")
    private Date registeredDate;
    
    @Column(name = "customerstatus")
    private String customerstatus;
    
    @Column(name = "customertierid")
    private String customerTierId;
    
    @Column(name = "documents")
    private String documents;
    
    @Column(name = "customertypeid")
    private String customerTypeId;
    
    @Column(name = "customerdob")
    private Date customerDob;
    
    @Column(name = "customertitleid")
    private String customertitleid;
    
    @Column(name = "customergender")
    private String customerGender;
    
    @Column(name = "customerlanguagepreference")
    private String customerLanguagePreference;
    
    @Column(name = "customerlocationlongtitude")
    private Float customerLocationLongtitude;
    
    @Column(name = "custtypeid")
    private String custTypeId;

    
    
    /*get the Customer Id*/
    public String getId() {
        return customerId;
    }

    /*get the Customer Id*/
    public void setId(String id) {
        this.customerId = id;
    }
    public String getCustomerId() {
        return customerId;
    }

    /*get the Customer Id*/
    public void setCustomerId(String id) {
        this.customerId = id;
    }

    /*get the Customer Name*/
    public String getCustomerName() {
        return customerName;
    }

    /*set the Customer Name */
    public void setCustomerName(String cusName) {
        this.customerName = cusName;
    }

    /*get the Complete City Address*/
    public String getCustomerAddress() {
        return customerAddress;
    }

    /*set the Complete City Address*/
    public void setCustomerAddress(String address) {
        this.customerAddress = address;
    }

    /*get the Customer Contact Email*/
    public String getCustomerContactEmail() {
        return customerContactEmail;
    }

    /*set the Customer Contact Email*/
    public void setCustomerContactEmail(String cus_cont_email) {
        this.customerContactEmail = cus_cont_email;
    }

    /*get the Customer Contact Phone*/
    public String getCustomerContactPhone() {
        return customerContactPhone;
    }

    /*set the Customer Contact Phone*/
    public void setCustomerContactPhone(String cus_cont_phone) {
        this.customerContactPhone = cus_cont_phone;
    }

    
    /*get the Customer Contact Social*/
    public String getContactContactSocial() {
        return contactContactSocial;
    }

    /*set the Customer Contact Social*/
    public void setContactContactSocial(String cus_cont_social) {
        this.contactContactSocial = cus_cont_social;
    }
    
    /*get the Customer Location Latitude*/
    public Double getCustomerLocationLatitude() {
        return customerLocationLatitude;
    }

    /*set the Customer Location Latitude*/
    public void setcustomerLocationLatitude(Double cus_loc_lat) {
        this.customerLocationLatitude = cus_loc_lat;
    }

    /*get the Customer Contact Social*/
    public String getLinkedCustomer() {
        return linkedCustomer;
    }

    /*set the Customer Linked Customers*/
    public void setLinkedCustomer(String cus_link_customer) {
        this.linkedCustomer = cus_link_customer;
    }

    /*get the Customer Contact Social*/
    public Integer getCustomerValue() {
        return customerValue;
    }

    /*set the Customer Linked Customers*/
    public void setCustomerValue(Integer cus_value) {
        this.customerValue = cus_value;
    }
    

    /*get the Customer Contact Social*/
    public Date getRegisteredDate() {
        return registeredDate;
    }

    /*set the Customer Linked Customers*/
    public void setRegisteredDate(Date cus_reg_date) {
        this.registeredDate = cus_reg_date;
    }
    
    
    /*get the Customer Contact Social*/
    public String getCustomerStatus() {
        return customerstatus;
    }

    /*set the Customer Linked Customers*/
    public void setCustomerStatus(String cus_status) {
        this.customerstatus = cus_status;
    }
    
    /*get the Customer Contact Social*/
    public String getCustomerTierId() {
        return customerTierId;
    }

    /*set the Customer Linked Customers*/
    public void setCustomerTierId(String cus_tier_id) {
        this.customerTierId = cus_tier_id;
    }
    
    
    /*get the Customer Contact Social*/
    public String getDocuments() {
        return documents;
    }

    /*set the Customer Linked Customers*/
    public void setDocuments(String cus_documents) {
        this.customerTierId = cus_documents;
    }
    
    /*get the Customer Contact Social*/
    public String getCustomerTypeId() {
        return customerTypeId;
    }

    /*set the Customer Linked Customers*/
    public void setCustomerTypeId(String cus_type_id) {
        this.customerTypeId = cus_type_id;
    }
    
    /*get the Customer Contact Social*/
    public Date getCustomerDob() {
        return customerDob;
    }

    /*set the Customer Linked Customers*/
    public void setCustomerDob(Date cus_date) {
        this.customerDob = cus_date;
    }
    
    /*get the Customer Contact Social*/
    public String getCustomerTitleId() {
        return customertitleid;
    }

    /*set the Customer Linked Customers*/
    public void setCustomerTitleId(String cus_tiltle_id) {
        this.customertitleid = cus_tiltle_id;
    }
    
    /*get the Customer Contact Social*/
    public String getCustomerGender() {
        return customerGender;
    }

    /*set the Customer Linked Customers*/
    public void setCustomerGender(String cus_gender) {
        this.customerGender = cus_gender;
    }
    
    /*get the Customer Contact Social*/
    public String getCustomerLanguagePreference() {
        return customerLanguagePreference;
    }

    /*set the Customer Linked Customers*/
    public void setCustomerLanguagePreference(String cus_lang_pref) {
        this.customerLanguagePreference = cus_lang_pref;
    }
    
    /*get the Customer Contact Social*/
    public Float getcustomerLocationLongtitude() {
        return customerLocationLongtitude;
    }

    /*set the Customer Linked Customers*/
    public void setcustomerLocationLongtitude(Float cus_lac_lat) {
        this.customerLocationLongtitude = cus_lac_lat;
    }
    /*get the Customer Contact Social*/
    public String getCustTypeId() {
        return custTypeId;
    }

    /*set the Customer Linked Customers*/
    public void setcustTypeId(String cus_lac_lat) {
        this.custTypeId = cus_lac_lat;
    }
    

}
