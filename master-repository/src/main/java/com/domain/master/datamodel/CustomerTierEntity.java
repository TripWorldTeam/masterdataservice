package com.domain.master.datamodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "customertiermaster")
public class CustomerTierEntity extends BaseEntity {

    @Column(name = "cust_tierid")
    @Id
    private Long customerTierId;

    @Column(name = "cust_tiername")
    private String customerTierName;

    @Column(name = "customerid")
    
    private String customerId;

    public Long getId() {
        return customerTierId;
    }

    public void setId(Long id) {
        this.customerTierId = id;
    }

    public String getCustomerTierName() {
        return customerTierName;
    }

    public void setCustomerTierName(String countryName) {
        this.customerTierName = countryName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Long getCustomerTierId() {
        return customerTierId;
    }

    public void setCustomerTierId(Long customerTierId) {
        this.customerTierId = customerTierId;
    }

}
