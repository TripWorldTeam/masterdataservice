package com.domain.master.datamodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "customertype_master")
public class CustomerTypeEntity extends BaseEntity {

    @Column(name = "custtypeid")
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long custTypeId;

    @Column(name = "custtypedesc")
    private String custTypeDesc;

    

    public Long getId() {
        return custTypeId;
    }

    public void setId(Long id) {
        this.custTypeId = id;
    }

    public String getCustTypeDesc() {
        return custTypeDesc;
    }

    public void setCustTypeDesc(String countryName) {
        this.custTypeDesc = countryName;
    }

    public Long getCustTypeId() {
        return custTypeId;
    }

    public void setCustTypeId(Long customerTypeId) {
        this.custTypeId = customerTypeId;
    }

}
