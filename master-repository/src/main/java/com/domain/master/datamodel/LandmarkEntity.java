/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.datamodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Query;
import javax.persistence.NamedQuery;


@Entity
@Table(name = "landmark_master")

/**
 *
 * @author root1 setting up the City table Entities
 */
public class LandmarkEntity extends BaseEntity {

    @Column(name = "landmarkid")
    @Id
    private String landmarkId;

    @Column(name = "landmarkdescription")
    private String landmarkDescription;

    @Column(name = "landmarktypeid")
    private String landmarkTypeId;
    

    @Column(name = "areaid")
    private String areaId;

    @Column(name = "latitude")
    private Float latitude;
    
    @Column(name = "longtitude")
    private Float longtitude;

    /*get the Landmark Id*/
    public String getId() {
        return landmarkId;
    }

    /*get the Landmark Id*/
    public void setId(String id) {
        this.landmarkId = id;
    }

    /*get the Country Id*/
    public String getLandmarkDescription() {
        return landmarkDescription;
    }

    /*set the Country Id*/
    public void setLandmarkDescription(String landDescription) {
        this.landmarkDescription = landDescription;
    }

    /*get the landmarkTypeId*/
    public String getLandmarkTypeId() {
        return landmarkTypeId;
    }

    /*set the landmarkTypeId*/
    public void setLandmarkTypeId(String landmarkTypeId) {
        this.landmarkTypeId = landmarkTypeId;
    }

    /*get the Area Id*/
    public String getAreaId() {
        return areaId;
    }

    /*set the Area Id*/
    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    /*get the latitude */
    public Float getLatitude() {
        return latitude;
    }

    /*set the latitude*/
    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLandmarkId() {
        return longtitude;
    }

    public void setLandmarkId(Float longtitude) {
        this.longtitude = longtitude;
    }

}
