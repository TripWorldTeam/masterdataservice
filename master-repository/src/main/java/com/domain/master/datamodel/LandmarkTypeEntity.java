package com.domain.master.datamodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "landmarktype_master")
public class LandmarkTypeEntity extends BaseEntity {

    @Column(name = "landmarktypeid")
    @Id 
    private String landmarkTypeId;

    @Column(name = "landmarkdescription")
    private String landmarkDescription;

    

    public String getId() {
        return landmarkTypeId;
    }

    public void setId(String id) {
        this.landmarkTypeId = id;
    }

    public String getCustTypeDesc() {
        return landmarkDescription;
    }

    public void setCustTypeDesc(String typeDesc) {
        this.landmarkDescription = typeDesc;
    }

    public String getLandmarkTypeId() {
        return landmarkTypeId;
    }

    public void setLandmarkTypeId(String landmarkTypeId) {
        this.landmarkTypeId = landmarkTypeId;
    }

}
