/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.datamodel;

import java.util.Date;
import javafx.scene.text.Text;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Query;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;


@Entity
@Table(name = "partners")

/**
 *
 * @author root1 setting up the City table Entities
 */
public class PartnerEntity extends BaseEntity {

    @Column(name = "partnerid")
    @Id
    private String partnerId;

    @Column(name = "partnername")
    private String partnerName;

    @Column(name = "partneraddress")
    private String partnerAddress;

    @Column(name = "partnercitycode")
    private String partnerCityCode;

    @Column(name = "partnercountrycode")
    private String partnerCountryCode;
    
    @Column(name = "partnerlocation")
    private String partnerLocation;
    
    @Column(name = "partner_hq_address")
    private Double partner_Hq_Address;
    
    @Column(name = "partnertypeid")
    private String partnerTypeId;
    
    @Column(name = "partnervalue")
    private Integer partnerValue;

    @Column(name = "partnerstatus")
    private String partnerStatus;
    
    @Column(name = "partneronboardingdate")
    private Date partnerOnBoardingDate;

    @Column(name = "partnerterminationdate")
    private Date partnerTerminationDate;
    
    @Column(name = "partnerterminationreason")
    private String partnerTerminationReason;
    
    
    /*get the Partner Id*/
    public String getId() {
        return partnerId;
    }

    /*get the Partner Id*/
    public void setId(String id) {
        this.partnerId = id;
    }
    public String getPartnerId() {
        return partnerId;
    }

    /*get the Partner Id*/
    public void setPartnerId(String id) {
        this.partnerId = id;
    }

    /*get the Partner Name*/
    public String getPartnerName() {
        return partnerName;
    }

    /*set the Partner Name */
    public void setPartnerName(String cusName) {
        this.partnerName = cusName;
    }

    /*get the Complete City Address*/
    public String getPartnerAddress() {
        return partnerAddress;
    }

    /*set the Complete City Address*/
    public void setPartnerAddress(String address) {
        this.partnerAddress = address;
    }

    /*get the PartnerCityCode*/
    public String getPartnerCityCode() {
        return partnerCityCode;
    }

    /*set the Partner Contact Email*/
    public void setPartnerCityCode(String partnerCityCode) {
        this.partnerCityCode = partnerCityCode;
    }

    /*get the Partner Contact Phone*/
    public String getPartnerCountryCode() {
        return partnerCountryCode;
    }

    /*set the Partner Contact Phone*/
    public void setPartnerCountryCode(String partnerCountryCode) {
        this.partnerCountryCode = partnerCountryCode;
    }

    
    /*get the Partner Location*/
    public String getPartnerLocation() {
        return partnerLocation;
    }

    /*set the Partner Location*/
    public void setPartnerLocation(String partnerLocation) {
        this.partnerLocation = partnerLocation;
    }
    
    /*get the Partner Location Latitude*/
    public Double getPartnerHqAddress() {
        return partner_Hq_Address;
    }

    /*set the Partner Location Latitude*/
    public void setPartnerHqAddress(Double partner_hq_address) {
        this.partner_Hq_Address = partner_hq_address;
    }

    /*get the Partner Type ID*/
    public String getPartnerTypeId() {
        return partnerTypeId;
    }

    /*set the Partner Type ID*/
    public void setPartnerTypeId(String partnerTypeId) {
        this.partnerTypeId = partnerTypeId;
    }

    /*get the Partner Contact Social*/
    public Integer getPartnerValue() {
        return partnerValue;
    }

    /*set the Partner Linked Partners*/
    public void setPartnerValue(Integer partnerValue) {
        this.partnerValue = partnerValue;
    }
    
    /*get the Partner Contact Social*/
    public String getPartnerStatus() {
        return partnerStatus;
    }

    /*set the Partner Linked Partners*/
    public void setPartnerStatus(String cus_status) {
        this.partnerStatus = cus_status;
    }
    
    /*get the Partner Contact Social*/
    public Date getPartnerOnBoardingDate() {
        return partnerOnBoardingDate;
    }

    /*set the Partner Linked Partners*/
    public void setPartnerOnBoardingDate(Date partnerOnBoardingDate) {
        this.partnerOnBoardingDate = partnerOnBoardingDate;
    }
    /*get the Partner Contact Social*/
    public Date getPartnerTerminationDate() {
        return partnerTerminationDate;
    }

    /*set the Partner Linked Partners*/
    public void setPartnerTerminationDate(Date partnerTerminationDate) {
        this.partnerTerminationDate = partnerTerminationDate;
    }
    
    /*get the Partner Contact Social*/
    public String getPartnerTerminationReason() {
        return partnerTerminationReason;
    }

    /*set the Partner Linked Partners*/
    public void setPartnerTerminationReason(String partnerTerminationReason) {
        this.partnerTerminationReason = partnerTerminationReason;
    }
    

}
