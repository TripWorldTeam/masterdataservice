/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.datamodel;

import com.domain.master.masterservice.dto.Partner;
import java.util.Date;
import javafx.scene.text.Text;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Query;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;


@Entity
@Table(name = "productcategory_master")

/**
 *
 * @author root1 setting up the City table Entities
 */
public class ProductCategoryEntity extends BaseEntity {

    @Column(name = "productcategoryid")
    @Id
    private String productCategoryId;

    @Column(name = "productcategorydescription")
    private String productCategoryDescription;

    @Column(name = "productcategorystartdate")
    private Date productCategoryStartDate;

    @Column(name = "productcategorystatus")
//    @ManyToOne
    private String productCategoryStatus;

    @Column(name = "productid")
    private String productId;
    
   
    
    /*get the Product Id*/
    public String getId() {
        return productCategoryId;
    }

    /*get the Product Id*/
    public void setId(String id) {
        this.productCategoryId = id;
    }
    
    public String getProductCategoryId() {
        return productCategoryId;
    }

    /*get the Product Id*/
    public void setProductCategoryId(String id) {
        this.productCategoryId = id;
    }
    
    public String getProductId() {
        return productId;
    }

    /*get the Product Id*/
    public void setProductId(String id) {
        this.productId = id;
    }

    /*get the Product Name*/
    public String getProductCategoryDescription() {
        return productCategoryDescription;
    }

    /*set the Product Name */
    public void setProductCategoryDescription(String productName) {
        this.productCategoryDescription = productName;
    }

    /*get the Complete City Address*/
    public Date getProductCategoryStartDate() {
        return productCategoryStartDate;
    }

    /*set the Complete City Address*/
    public void setProductCategoryStartDate(Date date) {
        this.productCategoryStartDate = date;
    }

    /*get the ProductCityCode*/
    public String getProductCategoryStatus() {
        return productCategoryStatus;
    }

    /*set the Product Contact Email*/
    public void setProductCategoryStatus(String productCategoryStatus) {
        this.productCategoryStatus = productCategoryStatus;
    }

    

}
