/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.datamodel;

import com.domain.master.masterservice.dto.Partner;
import java.util.Date;
import javafx.scene.text.Text;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Query;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;


@Entity
@Table(name = "product")

/**
 *
 * @author root1 setting up the City table Entities
 */
public class ProductEntity extends BaseEntity {

    @Column(name = "productid")
    @Id
    private String productId;

    @Column(name = "productname")
    private String productName;

    @Column(name = "productdescription")
    private String productDescription;

    @Column(name = "partnerid")
//    @ManyToOne
    private String partnerId;

    @Column(name = "producttypeid")
    private Integer productTypeId;
    
    @Column(name = "productspecification")
    private String productSpecification;
    
    @Column(name = "productcategoryid")
    private String productCategoryId;
    
    @Column(name = "productamenitiesgroup")
    private Integer productAmenitiesGroup;
    
    @Column(name = "productimage")
    private String productImage;

    @Column(name = "productqty")
    private Integer productQty;
    
   
    
    /*get the Product Id*/
    public String getId() {
        return productId;
    }

    /*get the Product Id*/
    public void setId(String id) {
        this.productId = id;
    }
    public String getProductId() {
        return productId;
    }

    /*get the Product Id*/
    public void setProductId(String id) {
        this.productId = id;
    }

    /*get the Product Name*/
    public String getProductName() {
        return productName;
    }

    /*set the Product Name */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /*get the Complete City Address*/
    public String getProductDescription() {
        return productDescription;
    }

    /*set the Complete City Address*/
    public void setProductDescription(String desciption) {
        this.productDescription = desciption;
    }

    /*get the ProductCityCode*/
    public String getpartnerId() {
        return partnerId;
    }

    /*set the Product Contact Email*/
    public void setPartnerId(String partnerCityCode) {
        this.partnerId = partnerCityCode;
    }

    /*get the Product Contact Phone*/
    public Integer getProductTypeId() {
        return productTypeId;
    }

    /*set the Product Contact Phone*/
    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    
    /*get the Product Location*/
    public String getProductSpecification() {
        return productSpecification;
    }

    /*set the Product Location*/
    public void setProductSpecification(String productSpecification) {
        this.productSpecification = productSpecification;
    }
    
    /*get the Product Location Latitude*/
    public String getProductCategoryId() {
        return productCategoryId;
    }

    /*set the Product Location Latitude*/
    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

   

    /*get the Product Contact Social*/
    public Integer getProductAmenitiesGroup() {
        return productAmenitiesGroup;
    }

    /*set the Product Linked Products*/
    public void setProductAmenitiesGroup(Integer productAmenitiesGroup) {
        this.productAmenitiesGroup = productAmenitiesGroup;
    }
    
    /*get the Product Contact Social*/
    public String getProductImage() {
        return productImage;
    }

    /*set the Product Linked Products*/
    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }
    
    
    /*get the Product Contact Social*/
    public Integer getProductQty() {
        return productQty;
    }

    /*set the Product Linked Products*/
    public void setProductQty(int productQty) {
        this.productQty = productQty;
    }
    

}
