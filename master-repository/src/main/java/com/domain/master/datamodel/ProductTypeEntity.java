package com.domain.master.datamodel;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "producttype_master")
public class ProductTypeEntity extends BaseEntity {

    @Column(name = "producttypeid")
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long productTypeId;

    @Column(name = "producttypedescription")
    private String productTypeDescription;
    
    @Column(name = "startdate")
    private Date startDate;
    
    @Column(name = "producttypestatus")
    private String productTypeStatus;

    

    public Long getId() {
        return productTypeId;
    }

    public void setId(Long id) {
        this.productTypeId = id;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long customerTypeId) {
        this.productTypeId = customerTypeId;
    }
    
    public String getProductTypeDescription() {
        return productTypeDescription;
    }

    public void setProductTypeDescription(String prodDesc) {
        this.productTypeDescription = prodDesc;
    }
    
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public String getproductTypeStatus() {
        return productTypeStatus;
    }

    public void setproductTypeStatus(String proStatus) {
        this.productTypeStatus = proStatus;
    }

   

}
