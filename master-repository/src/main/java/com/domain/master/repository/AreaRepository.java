package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.AreaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import org.hibernate.annotations.ParamDef;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.annotation.QueryAnnotation;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.querydsl.binding.QuerydslPredicate;

@Configuration
@EnableJpaRepositories("com.acme.repositories")
@Repository("AreaRepository")
public interface AreaRepository extends JpaRepository<AreaEntity, Long> {
    AreaEntity findByAreaName(String areaName);
    AreaEntity findByAreaId(String areaId);
}
