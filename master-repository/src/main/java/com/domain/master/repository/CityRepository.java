package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.CityEntity;
import java.util.List;
import org.hibernate.annotations.ParamDef;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.annotation.QueryAnnotation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.repository.query.Param;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

@Configuration
@EnableJpaRepositories("com.acme.repositories")
@Repository("cityRepository")
public interface CityRepository extends JpaRepository<CityEntity, Long> {

    CityEntity findByCityName(String city);
    CityEntity findByCityId(Long cityId);
    
//    @Query("SELECT u FROM crs_cities u WHERE u.country_id = ?1")
//    CityEntity findCitiesByCountryId(Integer countryId);
    CityEntity getCitiesByCountryId(Long cntryId);
//	CityEntity findByCityName( String city);

    /**
     *
     * @param city
     * @return
     */
//    @Query(value="Select * from crs_cities  where city_name Like '%:city%'", nativeQuery = true)
//	CityEntity findByCityNameLike(String city);
//    @Query("Select c.cityId, c.cityName from CityEntity c where c.cityName like ?1")
    List<CityEntity> findByCityNameContaining(String city);

//    public CityEntity findAllById(Long cntryId);

}
