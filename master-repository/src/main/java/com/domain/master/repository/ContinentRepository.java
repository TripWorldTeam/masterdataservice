package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.ContinentEntity;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.acme.repositories")
@Repository("continentRepository")

public interface ContinentRepository extends JpaRepository<ContinentEntity, Long>{
	
	ContinentEntity findByContinentName(String contName);
	ContinentEntity findByContinentId(String contId);
	
}

