package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.CountryEntity;

@Repository("countryRepository")
public interface CountryRepository extends JpaRepository<CountryEntity, Long>{
	
	CountryEntity findByCountryCode(String countryCode);
	
}
