package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.CustomerEntity;
import java.util.List;

@Repository("customerRepository")
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long>{
	/**
     * @param customerName
     * @return */
	CustomerEntity findByCustomerName(String customerName);
	CustomerEntity findByCustomerId(String customerID);
        List<CustomerEntity> findByCustomerNameContaining(String customer);
	
}
