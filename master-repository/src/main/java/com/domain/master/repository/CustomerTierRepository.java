package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.CustomerTierEntity;
import java.util.List;

@Repository("customerTierRepository")
public interface CustomerTierRepository extends JpaRepository<CustomerTierEntity, Long> {

    CustomerTierEntity findByCustomerTierName(String tierName);
    CustomerTierEntity findByCustomerId(String customerId);

    CustomerTierEntity findByCustomerTierId(Long tierId);

    /**
     *
     * @param city
     * @return
     */

    List<CustomerTierEntity> findByCustomerTierNameContaining(String city);

}
