package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.CustomerTypeEntity;
import java.util.List;

@Repository("customerTypeRepository")
public interface CustomerTypeRepository extends JpaRepository<CustomerTypeEntity, Long> {


    CustomerTypeEntity findByCustTypeId(Long typeId);

    /**
     *
     * @param tierDesc
     * @return
     */

    List<CustomerTypeEntity> findByCustTypeDescContaining(String typeDesc);

}
