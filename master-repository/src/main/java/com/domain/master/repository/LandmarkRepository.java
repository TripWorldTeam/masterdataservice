package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.LandmarkEntity;
import java.util.List;
import org.hibernate.annotations.ParamDef;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.annotation.QueryAnnotation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.repository.query.Param;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

@Configuration
@EnableJpaRepositories("com.acme.repositories")
@Repository("landmarkRepository")
public interface LandmarkRepository extends JpaRepository<LandmarkEntity, Long> {

//    LandmarkEntity findByCityName(String city);
//    LandmarkEntity findByCityId(Long cityId);
//    
////    @Query("SELECT u FROM crs_cities u WHERE u.country_id = ?1")
////    LandmarkEntity findCitiesByCountryId(Integer countryId);
//    LandmarkEntity getCitiesByCountryId(Long cntryId);
////	LandmarkEntity findByCityName( String city);
//
//    /**
//     *
//     * @param city
//     * @return
//     */
////    @Query(value="Select * from crs_cities  where city_name Like '%:city%'", nativeQuery = true)
////	LandmarkEntity findByCityNameLike(String city);
////    @Query("Select c.cityId, c.cityName from LandmarkEntity c where c.cityName like ?1")
//    List<LandmarkEntity> findByCityNameContaining(String city);

//    public LandmarkEntity findAllById(Long cntryId);

}
