package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.LandmarkTypeEntity;
import java.util.List;

@Repository("landmarkTypeRepository")
public interface LandmarkTypeRepository extends JpaRepository<LandmarkTypeEntity, Long> {


    LandmarkTypeEntity findByLandmarkTypeId(String typeId);

    /**
     *
     * @param tierDesc
     * @return
     */

    List<LandmarkTypeEntity> findByLandmarkDescriptionContaining(String typeDesc);

}
