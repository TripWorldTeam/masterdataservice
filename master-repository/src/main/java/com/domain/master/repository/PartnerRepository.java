package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.PartnerEntity;
import java.util.List;

@Repository("partnerRepository")
public interface PartnerRepository extends JpaRepository<PartnerEntity, Long>{
	/**
     * @param partnerName
     * @return */
	PartnerEntity findByPartnerName(String partnerName);
	PartnerEntity findByPartnerId(String partnerID);
        List<PartnerEntity> findByPartnerNameContaining(String partner);
	
}
