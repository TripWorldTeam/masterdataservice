package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.ProductCategoryEntity;
import java.util.List;

@Repository("productCategoryRepository")
public interface ProductCategoryRepository extends JpaRepository<ProductCategoryEntity, Long>{
	/**
     * @param productName
     * @return */
	ProductCategoryEntity findByProductCategoryId(String productID);
        
	
}
