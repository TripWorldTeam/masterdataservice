package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.ProductEntity;
import java.util.List;

@Repository("productRepository")
public interface ProductRepository extends JpaRepository<ProductEntity, Long>{
	/**
     * @param productName
     * @return */
	ProductEntity findByProductName(String productName);
	ProductEntity findByProductId(String productID);
        List<ProductEntity> findByProductNameContaining(String product);
        List<ProductEntity> findByPartnerId(String partnerId);
	
}
