package com.domain.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.domain.master.datamodel.ProductTypeEntity;
import java.util.List;

@Repository("productTypeRepository")
public interface ProductTypeRepository extends JpaRepository<ProductTypeEntity, Long> {


    ProductTypeEntity findByProductTypeId(Long typeId);

    /**
     *
     * @param tierDesc
     * @return
     */

    List<ProductTypeEntity> findByProductTypeDescriptionContaining(String typeDesc);

}
