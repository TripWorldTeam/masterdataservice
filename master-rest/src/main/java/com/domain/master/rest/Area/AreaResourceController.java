/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.rest.Area;

import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.AreaMasterDataService;
import com.domain.master.masterservice.dto.AreaM;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 *
 * @author root1
 */
@RestController("AreaMasterController")
@EnableAutoConfiguration
@RequestMapping("/area")

public class AreaResourceController {

    @Autowired
    AreaMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<AreaM>> getAllArea() throws AppException {
        try {
            List<AreaM> areaToList = null;
            areaToList = mastSrvc.getAllArea();
            return new ResponseEntity<List<AreaM>>(areaToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllArea Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/name/{areaName}")
    public ResponseEntity<AreaM> getAreaName(@PathVariable String areaName) throws AppException {
        try {
            AreaM area = mastSrvc.getAreaName(areaName);
            return new ResponseEntity<AreaM>(area, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllArea Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/{areaId}")
    public ResponseEntity<AreaM> getAreaId(@PathVariable String areaId) throws AppException {
        try {
            AreaM area = mastSrvc.getAreaId(areaId);
            return new ResponseEntity<AreaM>(area, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllArea Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
