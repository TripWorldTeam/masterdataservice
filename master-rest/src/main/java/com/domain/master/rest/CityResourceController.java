/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.rest;

import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.CityMasterDataService;
import com.domain.master.masterservice.dto.City;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 *
 * @author root1
 */
@RestController("cityMasterController")
@EnableAutoConfiguration
@RequestMapping("/cities")
public class CityResourceController {

    @Autowired
    CityMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<City>> getAllCities() throws AppException {
        try {
            List<City> cityToList = null;
            cityToList = mastSrvc.getAllCities();
            return new ResponseEntity<List<City>>(cityToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllCities Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/by_name/{cityName}")
    public ResponseEntity<City> getCityName(@PathVariable String cityName) throws AppException {

        try {
            City city = null;
            city = mastSrvc.getCityName(cityName);
            return new ResponseEntity<City>(city, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCityName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/like/{cityName}")
    public ResponseEntity<List<City>> getCityNameLike(@PathVariable String cityName) throws AppException {
        try {
            List<City> cityToList = null;
            cityToList = mastSrvc.getCityNameLike(cityName);
            return new ResponseEntity<List<City>>(cityToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCityNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/{cityId}")
    public ResponseEntity<City> getCityId(@PathVariable Long cityId) throws AppException {
        try {
            City city = mastSrvc.getCityId(cityId);
            return new ResponseEntity<City>(city, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCityId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/for_country/{countryId}")
    public ResponseEntity<City> getCountryId(@PathVariable Long countryId) throws AppException {
        try {
            City city = mastSrvc.getCitiesByCountryId(countryId);
            return new ResponseEntity<City>(city, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCountryId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
