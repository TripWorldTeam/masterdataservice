package com.domain.master.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.IMasterDataService;
import com.domain.master.masterservice.dto.CountryTO;

@RestController("masterController")
@RequestMapping("/country")
public class CountryResourceController {
	
	@Autowired
	IMasterDataService mastSrvc;
	
	@GetMapping("list")
	public ResponseEntity<List<CountryTO>> getAllCountries(){
		//ResponseEntity<List<CountryTO>> entList = new ResponseEntity<List<CountryTO>>();
		List<CountryTO> cntryToList = mastSrvc.getAllCountries();
		return new ResponseEntity<List<CountryTO>>(cntryToList, HttpStatus.OK);
	}
	
	@GetMapping(value = "{countryCode}")
	public ResponseEntity<CountryTO> getCountry(@PathVariable String countryCode) {
		CountryTO countryTO = mastSrvc.getCountry(countryCode);
		return new ResponseEntity<CountryTO>(countryTO, HttpStatus.OK);
	}

}
