/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.rest;


import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.CurrencyMasterDataService;
import com.domain.master.masterservice.dto.Currency;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 *
 * @author root1
 */
@RestController("curencyMasterController")
@EnableAutoConfiguration
@RequestMapping("/currency")

public class CurrencyResourceController {

    @Autowired
    CurrencyMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<Currency>> getAllCurrencies() throws AppException {
        try {
            List<Currency> currToList = null;
            try {
                currToList = mastSrvc.getAllCurrencies();

            } catch (Exception e) {
                e.getMessage();
            }
            return new ResponseEntity<List<Currency>>(currToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllCurrencies Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

   

    @GetMapping(value = "/{currencyCode}")
    public ResponseEntity<Currency> getCurrencyCode(@PathVariable String currCode) throws AppException {
        try {
            Currency curr = null;
            curr = mastSrvc.getCurrencyCode(currCode);
            return new ResponseEntity<Currency>(curr, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCurrencyCode Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
