package com.domain.master.rest;

import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.CustomerTierMasterDataService;
import com.domain.master.masterservice.dto.CustomerTier;

@RestController("customerTierMasterController")
@RequestMapping("/customerTier")
public class CustomerTierResourceController {

    @Autowired
    CustomerTierMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<CustomerTier>> getAllCustomerTier() throws AppException {
        try {
            List<CustomerTier> cTierToList = null;
            cTierToList = mastSrvc.getAllCustomerTier();
            return new ResponseEntity<List<CustomerTier>>(cTierToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllCities Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/by_name/{cTierName}")
    public ResponseEntity<CustomerTier> getCustomerTierName(@PathVariable String cTierName) throws AppException {

        try {
            CustomerTier cTier = null;
            cTier = mastSrvc.getCustomerTierName(cTierName);
            return new ResponseEntity<CustomerTier>(cTier, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCustomerTierName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/like/{cTierName}")
    public ResponseEntity<List<CustomerTier>> getCustomerTierNameLike(@PathVariable String cTierName) throws AppException {
        try {
            List<CustomerTier> cTierToList = null;
            cTierToList = mastSrvc.getCustomerTierNameLike(cTierName);
            return new ResponseEntity<List<CustomerTier>>(cTierToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCustomerTierNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/{cTierId}")
    public ResponseEntity<CustomerTier> getCustomerTierId(@PathVariable Long cTierId) throws AppException {
        try {
            CustomerTier cTier = mastSrvc.getCustomerTierId(cTierId);
            return new ResponseEntity<CustomerTier>(cTier, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCustomerTierId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/customer/{customerId}")
    public ResponseEntity<CustomerTier> getCustomerId(@PathVariable String customerId) throws AppException {
        try {
            CustomerTier cTier = mastSrvc.getCustomerId(customerId);
            return new ResponseEntity<CustomerTier>(cTier, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCountryId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
