package com.domain.master.rest;

import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.CustomerTypeMasterDataService;
import com.domain.master.masterservice.dto.CustomerType;

@RestController("customerTypeMasterController")
@RequestMapping("/customerType")
public class CustomerTypeResourceController {

    @Autowired
    CustomerTypeMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<CustomerType>> getAllCustomerType() throws AppException {
        try {
            List<CustomerType> cTypeToList = null;
            cTypeToList = mastSrvc.getAllCustomerType();
            return new ResponseEntity<List<CustomerType>>(cTypeToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllCities Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/like/{typeDesc}")
    public ResponseEntity<List<CustomerType>> getCustomerTypeDescLike(@PathVariable String typeDesc) throws AppException {
        try {
            List<CustomerType> cTypeToList = null;
            cTypeToList = mastSrvc.getCustTypeDescLike(typeDesc);
            return new ResponseEntity<List<CustomerType>>(cTypeToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCustomerTypeNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/{typeId}")
    public ResponseEntity<CustomerType> getCustomerTypeId(@PathVariable Long typeId) throws AppException {
        try {
            CustomerType cType = mastSrvc.getCustTypeId(typeId);
            return new ResponseEntity<CustomerType>(cType, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCustomerTypeId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    

}
