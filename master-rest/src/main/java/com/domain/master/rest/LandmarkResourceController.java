/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.rest;

import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.LandmarkMasterDataService;
import com.domain.master.masterservice.dto.Landmark;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 *
 * @author root1
 */
@RestController("landmarkMasterController")
@EnableAutoConfiguration
@RequestMapping("/landmark")
public class LandmarkResourceController {

    @Autowired
    LandmarkMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<Landmark>> getAllLandmark() throws AppException {
        try {
            List<Landmark> cityToList = null;
            cityToList = mastSrvc.getAllLandmark();
            return new ResponseEntity<List<Landmark>>(cityToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllCities Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

//    @GetMapping(value = "/by_name/{cityName}")
//    public ResponseEntity<Landmark> getLandmarkName(@PathVariable String cityName) throws AppException {
//
//        try {
//            Landmark city = null;
//            city = mastSrvc.getLandmarkName(cityName);
//            return new ResponseEntity<Landmark>(city, HttpStatus.OK);
//        } catch (Exception ex) {
//            System.out.println("getLandmarkName Exception !!! " + ex.getMessage());
//            throw new AppException();
//        }
//    }
//
//    @GetMapping(value = "/like/{cityName}")
//    public ResponseEntity<List<Landmark>> getLandmarkNameLike(@PathVariable String cityName) throws AppException {
//        try {
//            List<Landmark> cityToList = null;
//            cityToList = mastSrvc.getLandmarkNameLike(cityName);
//            return new ResponseEntity<List<Landmark>>(cityToList, HttpStatus.OK);
//        } catch (Exception ex) {
//            System.out.println("getLandmarkNameLike Exception !!! " + ex.getMessage());
//            throw new AppException();
//        }
//    }
//
//    @GetMapping(value = "/{cityId}")
//    public ResponseEntity<Landmark> getLandmarkId(@PathVariable Long cityId) throws AppException {
//        try {
//            Landmark city = mastSrvc.getLandmarkId(cityId);
//            return new ResponseEntity<Landmark>(city, HttpStatus.OK);
//        } catch (Exception ex) {
//            System.out.println("getLandmarkId Exception !!! " + ex.getMessage());
//            throw new AppException();
//        }
//    }

   

}
