package com.domain.master.rest;

import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.LandmarkTypeMasterDataService;
import com.domain.master.masterservice.dto.LandmarkType;

@RestController("landmarkTypeMasterController")
@RequestMapping("/landmarkType")
public class LandmarkTypeResourceController {

    @Autowired
    LandmarkTypeMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<LandmarkType>> getAllLandmarkType() throws AppException {
        try {
            List<LandmarkType> cTypeToList = null;
            cTypeToList = mastSrvc.getAllLandmarkType();
            return new ResponseEntity<List<LandmarkType>>(cTypeToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllLandmarkType Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/like/{typeDesc}")
    public ResponseEntity<List<LandmarkType>> getLandmarkDescriptionLike(@PathVariable String typeDesc) throws AppException {
        try {
            List<LandmarkType> cTypeToList = null;
            cTypeToList = mastSrvc.getLandmarkDescriptionLike(typeDesc);
            return new ResponseEntity<List<LandmarkType>>(cTypeToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getLandmarkDescriptionLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/{typeId}")
    public ResponseEntity<LandmarkType> getLandmarkTypeId(@PathVariable String typeId) throws AppException {
        try {
            LandmarkType cType = mastSrvc.getLandmarkTypeId(typeId);
            return new ResponseEntity<LandmarkType>(cType, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getLandmarkTypeId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    

}
