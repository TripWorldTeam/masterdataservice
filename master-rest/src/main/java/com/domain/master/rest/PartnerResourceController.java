/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.rest;


import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.PartnerMasterDataService;
import com.domain.master.masterservice.dto.Partner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 *
 * @author root1
 */
@RestController("partnerMasterController")
@EnableAutoConfiguration
@RequestMapping("/partner")

public class PartnerResourceController {

    @Autowired
    PartnerMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<Partner>> getAllPartner() throws AppException {
        try {
            List<Partner> partnerToList = mastSrvc.getAllPartner();
            return new ResponseEntity<List<Partner>>(partnerToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllPartner Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/name/{partnerName}")
    public ResponseEntity<Partner> getPartnerName(@PathVariable String partnerName) throws AppException {
        try {
            Partner partner = mastSrvc.getPartnerName(partnerName);
            return new ResponseEntity<Partner>(partner, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getPartnerName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/like/{partnerName}")
    public ResponseEntity<List<Partner>> getPartnerNameLike(@PathVariable String partnerName) throws AppException {

        try {
            List<Partner> partnerToList = mastSrvc.getPartnerNameLike(partnerName);
            return new ResponseEntity<List<Partner>>(partnerToList, HttpStatus.OK);

        } catch (Exception ex) {
            System.out.println("getPartnerNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }

    }

    @GetMapping(value = "/{partnerId}")
    public ResponseEntity<Partner> getPartnerId(@PathVariable String partnerId) throws AppException {
        try {
            Partner partner = mastSrvc.getPartnerId(partnerId);
            return new ResponseEntity<Partner>(partner, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getPartnerId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
