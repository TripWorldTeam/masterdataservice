/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.rest;


import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.ProductCategoryMasterDataService;
import com.domain.master.masterservice.dto.ProductCategory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 *
 * @author root1
 */
@RestController("productCategoryMasterController")
@EnableAutoConfiguration
@RequestMapping("/product_categories")

public class ProductCategoryResourceController {

    @Autowired
    ProductCategoryMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<ProductCategory>> getAllProductCategory() throws AppException {
        try {
            List<ProductCategory> productToList = mastSrvc.getAllProductCategory();
            return new ResponseEntity<List<ProductCategory>>(productToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllProductCategory Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    

    @GetMapping(value = "/{categoryId}")
    public ResponseEntity<ProductCategory> getProductCategoryId(@PathVariable String categoryId) throws AppException {
        try {
            ProductCategory product = mastSrvc.getProductCategoryId(categoryId);
            return new ResponseEntity<ProductCategory>(product, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getProductCategoryId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
