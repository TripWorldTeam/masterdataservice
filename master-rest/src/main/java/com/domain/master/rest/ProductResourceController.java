/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.rest;


import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.ProductMasterDataService;
import com.domain.master.masterservice.dto.Product;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 *
 * @author root1
 */
@RestController("productMasterController")
@EnableAutoConfiguration
@RequestMapping("/product")

public class ProductResourceController {

    @Autowired
    ProductMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<Product>> getAllProduct() throws AppException {
        try {
            List<Product> productToList = mastSrvc.getAllProduct();
            return new ResponseEntity<List<Product>>(productToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllProduct Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/name/{productName}")
    public ResponseEntity<Product> getProductName(@PathVariable String productName) throws AppException {
        try {
            Product product = mastSrvc.getProductName(productName);
            return new ResponseEntity<Product>(product, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getProductName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/like/{productName}")
    public ResponseEntity<List<Product>> getProductNameLike(@PathVariable String productName) throws AppException {

        try {
            List<Product> productToList = mastSrvc.getProductNameLike(productName);
            return new ResponseEntity<List<Product>>(productToList, HttpStatus.OK);

        } catch (Exception ex) {
            System.out.println("getProductNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }

    }

    @GetMapping(value = "/{productId}")
    public ResponseEntity<Product> getProductId(@PathVariable String productId) throws AppException {
        try {
            Product product = mastSrvc.getProductId(productId);
            return new ResponseEntity<Product>(product, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getProductId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
