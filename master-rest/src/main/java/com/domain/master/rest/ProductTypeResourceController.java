package com.domain.master.rest;

import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.ProductTypeMasterDataService;
import com.domain.master.masterservice.dto.ProductType;

@RestController("productTypeMasterController")
@RequestMapping("/productType")
public class ProductTypeResourceController {

    @Autowired
    ProductTypeMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<ProductType>> getAllProductType() throws AppException {
        try {
            List<ProductType> cTypeToList = null;
            cTypeToList = mastSrvc.getAllProductType();
            return new ResponseEntity<List<ProductType>>(cTypeToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllCities Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/like/{typeDesc}")
    public ResponseEntity<List<ProductType>> getProductTypeDescLike(@PathVariable String typeDesc) throws AppException {
        try {
            List<ProductType> cTypeToList = null;
            cTypeToList = mastSrvc.getProductTypeDescriptionLike(typeDesc);
            return new ResponseEntity<List<ProductType>>(cTypeToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getProductTypeNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/{typeId}")
    public ResponseEntity<ProductType> getProductTypeId(@PathVariable Long typeId) throws AppException {
        try {
            ProductType cType = mastSrvc.getProductTypeId(typeId);
            return new ResponseEntity<ProductType>(cType, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getProductTypeId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    

}
