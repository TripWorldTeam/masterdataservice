/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.rest.continent;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.ContinentMasterDataService;
import com.domain.master.masterservice.dto.Continent;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 *
 * @author root1
 */

@RestController("continentMasterController")
@EnableAutoConfiguration
@RequestMapping("/continent")

public class ContinentResourceController {
 
    @Autowired
	ContinentMasterDataService mastSrvc;
	
	@GetMapping("/list")
	public ResponseEntity<List<Continent>> getAllContinents(){
            List<Continent> contToList = null;
            try{
		contToList = mastSrvc.getAllContinents();
		
            }catch (Exception e){
                e.getMessage();
            }
            return new ResponseEntity<List<Continent>>(contToList, HttpStatus.OK);
	}
	
	@GetMapping(value = "/name/{contName}")
	public ResponseEntity<Continent> getContinentName(@PathVariable String contName) {
            Continent cont = null;
            try{
		cont = mastSrvc.getContinentName(contName);
            }catch (Exception e){
                e.getMessage();
            }
            return new ResponseEntity<Continent>(cont, HttpStatus.OK);
	}
        
	
        @GetMapping(value = "/{contId}")
	public ResponseEntity<Continent> getContinentId(@PathVariable String contId) {
            Continent cont = null;
            try{
		cont = mastSrvc.getContinentId(contId);
            }catch (Exception e){
                e.getMessage();
            }
            return new ResponseEntity<Continent>(cont, HttpStatus.OK);
	}
       
}
