/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.rest.customers;

import com.domain.master.interfaces.AppException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.master.interfaces.CustomerMasterDataService;
import com.domain.master.masterservice.dto.Customers;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 *
 * @author root1
 */
@RestController("customerMasterController")
@EnableAutoConfiguration
@RequestMapping("/customer")

public class CustomerResourceController {

    @Autowired
    CustomerMasterDataService mastSrvc;

    @GetMapping("/list")
    public ResponseEntity<List<Customers>> getAllCustomers() throws AppException {
        try {
            List<Customers> customerToList = mastSrvc.getAllCustomers();
            return new ResponseEntity<List<Customers>>(customerToList, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getAllCustomers Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/name/{customerName}")
    public ResponseEntity<Customers> getCustomerName(@PathVariable String customerName) throws AppException {
        try {
            Customers customer = mastSrvc.getCustomerName(customerName);
            return new ResponseEntity<Customers>(customer, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCustomerName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @GetMapping(value = "/like/{customerName}")
    public ResponseEntity<List<Customers>> getCustomerNameLike(@PathVariable String customerName) throws AppException {

        try {
            List<Customers> customerToList = mastSrvc.getCustomerNameLike(customerName);
            return new ResponseEntity<List<Customers>>(customerToList, HttpStatus.OK);

        } catch (Exception ex) {
            System.out.println("getCustomerNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }

    }

    @GetMapping(value = "/{customerId}")
    public ResponseEntity<Customers> getCustomerId(@PathVariable String customerId) throws AppException {
        try {
            Customers customer = mastSrvc.getCustomerId(customerId);
            return new ResponseEntity<Customers>(customer, HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println("getCustomerId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
