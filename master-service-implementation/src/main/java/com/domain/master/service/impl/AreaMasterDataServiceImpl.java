package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.AreaEntity;

import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.AreaMasterDataService;

import com.domain.master.masterservice.dto.AreaM;

import com.domain.master.repository.AreaRepository;
import org.springframework.beans.BeansException;

@Service("AreaMasterDataService")
public class AreaMasterDataServiceImpl implements AreaMasterDataService {

    @Autowired
    AreaRepository areaRepository;

    /**
     *
     * @return
     */
    @Override
    public List<AreaM> getAllArea() throws AppException {
        try {
            List<AreaM> areaToList = new ArrayList<AreaM>();
            List<AreaEntity> AreaEntityList = areaRepository.findAll();
            for (AreaEntity ent : AreaEntityList) {
                AreaM areaTo = new AreaM();
                BeanUtils.copyProperties(ent, areaTo);
                areaToList.add(areaTo);
            }
            return areaToList;
        } catch (BeansException ex) {
            System.out.println("getAllArea Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllArea Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @Override
    public AreaM getAreaName(String AreaName) throws AppException {
        try {
            AreaEntity areaEnt = areaRepository.findByAreaName(AreaName);
            AreaM areaTo = new AreaM();
            BeanUtils.copyProperties(areaEnt, areaTo);
            return areaTo;
        } catch (BeansException ex) {
            System.out.println("getAreaName Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAreaName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @Override
    public AreaM getAreaId(String areaId) throws AppException {
        try {
            AreaEntity areaEnt = areaRepository.findByAreaId(areaId);
            AreaM areaTo = new AreaM();
            BeanUtils.copyProperties(areaEnt, areaTo);
            return areaTo;
        } catch (BeansException ex) {
            System.out.println("getAreaMId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAreaMId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
