package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.CityEntity;

import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.CityMasterDataService;

import com.domain.master.masterservice.dto.City;

import com.domain.master.repository.CityRepository;
import org.springframework.beans.BeansException;

@Service("cityMasterDataService")
public class CityMasterDataServiceImpl implements CityMasterDataService {

    @Autowired
    CityRepository cityRepository;

    /**
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<City> getAllCities() throws AppException {
        try {
            List<City> cityToList = new ArrayList<City>();
            List<CityEntity> cityEntityList = cityRepository.findAll();
            for (CityEntity ent : cityEntityList) {
                City cityTo = new City();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;
        } catch (BeansException ex) {
            System.out.println("getAllCities Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllCities Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     * @param city
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public City getCityName(String city) throws AppException {
        try {
            CityEntity cityEnt = cityRepository.findByCityName(city);
            City cityTo = new City();
            BeanUtils.copyProperties(cityEnt, cityTo);
            return cityTo;
        } catch (BeansException ex) {
            System.out.println("getCityName Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCityName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     * @param city
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<City> getCityNameLike(String city) throws AppException {
        try {
            System.out.println("getCityNameLike !!! " + city);
            List<City> cityToList = new ArrayList<City>();
            List<CityEntity> cityEntityList;
            cityEntityList = cityRepository.findByCityNameContaining(city);
            for (CityEntity ent : cityEntityList) {
                City cityTo = new City();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;

        } catch (BeansException ex) {
            System.out.println("getCityNameLike Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCityNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     * @param cityId
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public City getCityId(Long cityId) throws AppException {
        // TODO Auto-generated method stub
        try {
            CityEntity cityEnt = cityRepository.findByCityId(cityId);
            City cityTo = new City();
            BeanUtils.copyProperties(cityEnt, cityTo);
            return cityTo;
        } catch (BeansException ex) {
            System.out.println("getCityId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCityId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     *
     * @param cntryId
     * @return
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public City getCitiesByCountryId(Long cntryId) throws AppException {
        try {
            CityEntity cityEnt = cityRepository.getCitiesByCountryId(cntryId);
            City cityTo = new City();
            BeanUtils.copyProperties(cityEnt, cityTo);
            return cityTo;
        } catch (BeansException ex) {
            System.out.println("getCitiesByCountryId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCitiesByCountryId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
