package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.ContinentEntity;

import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.ContinentMasterDataService;

import com.domain.master.masterservice.dto.Continent;

import com.domain.master.repository.ContinentRepository;
import org.springframework.beans.BeansException;

@Service("continentMasterDataService")
public class ContinentMasterDataServiceImpl implements ContinentMasterDataService {

    @Autowired
    ContinentRepository continentRepository;

     /**
     * @return ********************************
     */
    @Override
    public List<Continent> getAllContinents() throws AppException {
        try {
        List<Continent> contiToList = new ArrayList<Continent>();
        List<ContinentEntity> customerEntityList = continentRepository.findAll();
        for (ContinentEntity ent : customerEntityList) {
            Continent contTo = new Continent();
            BeanUtils.copyProperties(ent, contTo);
            contiToList.add(contTo);
        }
        return contiToList;
        } catch (BeansException ex) {
            System.out.println("getAllContinents Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllContinents Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
    /**
     * @param city
     * @return ********************************
     */
    @Override
    public Continent getContinentName(String city) throws AppException {
        try {
        ContinentEntity customerEnt = continentRepository.findByContinentName(city);
        Continent customerTo = new Continent();
        BeanUtils.copyProperties(customerEnt, customerTo);
        return customerTo;
        } catch (BeansException ex) {
            System.out.println("getContinentName Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getContinentName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
    /**
     * @param customerId
     * @return ********************************
     */
    @Override
    public Continent getContinentId(String customerId) throws AppException {
        try {
        ContinentEntity customerEnt = continentRepository.findByContinentId(customerId);
        Continent customerTo = new Continent();
        BeanUtils.copyProperties(customerEnt, customerTo);
        return customerTo;
        } catch (BeansException ex) {
            System.out.println("getContinentId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getContinentId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    

}
