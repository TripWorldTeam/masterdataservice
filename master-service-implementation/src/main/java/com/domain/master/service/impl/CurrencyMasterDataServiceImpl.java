package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.CurrencyEntity;

import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.CurrencyMasterDataService;

import com.domain.master.masterservice.dto.Currency;

import com.domain.master.repository.CurrencyRepository;
import org.springframework.beans.BeansException;

@Service("currencyMasterDataService")
public class CurrencyMasterDataServiceImpl implements CurrencyMasterDataService {

    @Autowired
    CurrencyRepository currRepository;

    /**
     *
     * @return
     */
    @Override
    public List<Currency> getAllCurrencies() throws AppException {
        try {
            List<Currency> currToList = new ArrayList<Currency>();
            List<CurrencyEntity> CurrencyEntityList = currRepository.findAll();
            for (CurrencyEntity ent : CurrencyEntityList) {
                Currency currTo = new Currency();
                BeanUtils.copyProperties(ent, currTo);
                currToList.add(currTo);
            }
            return currToList;
        } catch (BeansException ex) {
            System.out.println("getAllArea Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllArea Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     *
     * @param currCode
     * @return
     */
    @Override
    public Currency getCurrencyCode(String currCode) throws AppException {
        try {
        CurrencyEntity currEnt = currRepository.findByCurrencyCode(currCode);
            Currency currTo = new Currency();
        BeanUtils.copyProperties(currEnt, currTo);
            return currTo;
        } catch (BeansException ex) {
            System.out.println("getAreaId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAreaId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

}
