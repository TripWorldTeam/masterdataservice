package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.CustomerEntity;

import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.CustomerMasterDataService;

import com.domain.master.masterservice.dto.Customers;

import com.domain.master.repository.CustomerRepository;
import org.springframework.beans.BeansException;

@Service("customerMasterDataService")
public class CustomerMasterDataServiceImpl implements CustomerMasterDataService {

    @Autowired
    CustomerRepository customerRepository;

    /**
     *
     * @return
     */
    /**
     * @return ********************************
     */
    @Override
    public List<Customers> getAllCustomers() throws AppException {
        try {
            List<Customers> customerToList = new ArrayList<Customers>();
            List<CustomerEntity> customerEntityList = customerRepository.findAll();
            for (CustomerEntity ent : customerEntityList) {
                Customers customerTo = new Customers();
                BeanUtils.copyProperties(ent, customerTo);
                customerToList.add(customerTo);
            }
            return customerToList;
        } catch (BeansException ex) {
            System.out.println("getAreaId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAreaId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @Override
    public Customers getCustomerName(String city) throws AppException {
        try {
            CustomerEntity customerEnt = customerRepository.findByCustomerName(city);
            Customers customerTo = new Customers();
            BeanUtils.copyProperties(customerEnt, customerTo);
            return customerTo;
        } catch (BeansException ex) {
            System.out.println("getAreaId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAreaId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @Override
    public Customers getCustomerId(String customerId) throws AppException {
        try {
            CustomerEntity customerEnt = customerRepository.findByCustomerId(customerId);
            Customers customerTo = new Customers();
            BeanUtils.copyProperties(customerEnt, customerTo);
            return customerTo;
        } catch (BeansException ex) {
            System.out.println("getAreaId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAreaId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
    @Override
    public List<Customers> getCustomerNameLike(String customerName) throws AppException {
        try {
            System.out.println("getCityNameLike !!! " + customerName);
            List<Customers> cityToList = new ArrayList<Customers>();
            List<CustomerEntity> cityEntityList;
            cityEntityList =  customerRepository.findByCustomerNameContaining(customerName);
            for (CustomerEntity ent : cityEntityList) {
                Customers cityTo = new Customers();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;

        } catch (BeansException ex) {
            System.out.println("getCityNameLike Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCityNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }


}
