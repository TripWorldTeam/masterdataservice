package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.CountryEntity;
import com.domain.master.datamodel.CustomerEntity;
import com.domain.master.datamodel.CustomerTierEntity;
import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.CustomerTierMasterDataService;


import com.domain.master.masterservice.dto.Customers;
import com.domain.master.masterservice.dto.CustomerTier;


import com.domain.master.repository.CustomerRepository;
import com.domain.master.repository.CustomerTierRepository;
import org.springframework.beans.BeansException;

@Service("customerTierMasterDataService")
public class CustomerTierMasterDataServiceImpl implements CustomerTierMasterDataService {


    @Autowired
    CustomerTierRepository customerTierRepository;

    @Autowired
    CustomerRepository customerRepository;


    /**
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<CustomerTier> getAllCustomerTier() throws AppException {
        try {
            List<CustomerTier> cityToList = new ArrayList<CustomerTier>();
            List<CustomerTierEntity> cityEntityList = customerTierRepository.findAll();
            for (CustomerTierEntity ent : cityEntityList) {
                CustomerTier cityTo = new CustomerTier();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;
        } catch (BeansException ex) {
            System.out.println("getAllCustomerTier Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllCustomerTier Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     * @param city
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public CustomerTier getCustomerTierName(String city) throws AppException {
        try {
            CustomerTierEntity cityEnt = customerTierRepository.findByCustomerTierName(city);
            CustomerTier cityTo = new CustomerTier();
            BeanUtils.copyProperties(cityEnt, cityTo);
            return cityTo;
        } catch (BeansException ex) {
            System.out.println("getCustomerTierName Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCustomerTierName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     * @param city
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<CustomerTier> getCustomerTierNameLike(String city) throws AppException {
        try {
            System.out.println("getCustomerTierNameLike !!! " + city);
            List<CustomerTier> cityToList = new ArrayList<CustomerTier>();
            List<CustomerTierEntity> cityEntityList;
            cityEntityList = customerTierRepository.findByCustomerTierNameContaining(city);
            for (CustomerTierEntity ent : cityEntityList) {
                CustomerTier cityTo = new CustomerTier();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;

        } catch (BeansException ex) {
            System.out.println("getCustomerTierNameLike Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCustomerTierNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     * @param tierId
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public CustomerTier getCustomerTierId(Long tierId) throws AppException {
        // TODO Auto-generated method stub
        try {
            CustomerTierEntity cityEnt = customerTierRepository.findByCustomerTierId(tierId);
            CustomerTier cityTo = new CustomerTier();
            BeanUtils.copyProperties(cityEnt, cityTo);
            return cityTo;
        } catch (BeansException ex) {
            System.out.println("getCustomerTierId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCustomerTierId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
    
    /**
     * @param customerId
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public CustomerTier getCustomerId(String customerId) throws AppException {
        // TODO Auto-generated method stub
        try {
            CustomerTierEntity cityEnt = customerTierRepository.findByCustomerId(customerId);
            CustomerTier cityTo = new CustomerTier();
            BeanUtils.copyProperties(cityEnt, cityTo);
            return cityTo;
        } catch (BeansException ex) {
            System.out.println("getCustomerTierId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCustomerTierId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

   


   

   

}
