package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.CountryEntity;
import com.domain.master.datamodel.CustomerEntity;
import com.domain.master.datamodel.CustomerTypeEntity;
import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.CustomerTypeMasterDataService;


import com.domain.master.masterservice.dto.Customers;
import com.domain.master.masterservice.dto.CustomerType;


import com.domain.master.repository.CustomerRepository;
import com.domain.master.repository.CustomerTypeRepository;
import org.springframework.beans.BeansException;

@Service("customerTypeMasterDataService")
public class CustomerTypeMasterDataServiceImpl implements CustomerTypeMasterDataService {


    @Autowired
    CustomerTypeRepository customerTypeRepository;

    @Autowired
    CustomerRepository customerRepository;


    /**
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<CustomerType> getAllCustomerType() throws AppException {
        try {
            List<CustomerType> cityToList = new ArrayList<CustomerType>();
            List<CustomerTypeEntity> cityEntityList = customerTypeRepository.findAll();
            for (CustomerTypeEntity ent : cityEntityList) {
                CustomerType cityTo = new CustomerType();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;
        } catch (BeansException ex) {
            System.out.println("getAllCustomerType Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllCustomerType Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

   
    /**
     * @param city
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<CustomerType> getCustTypeDescLike(String cTypeDesc) throws AppException {
        try {
            System.out.println("getCustomerTypeNameLike !!! " + cTypeDesc);
            List<CustomerType> cityToList = new ArrayList<CustomerType>();
            List<CustomerTypeEntity> cityEntityList;
            cityEntityList = customerTypeRepository.findByCustTypeDescContaining(cTypeDesc);
            for (CustomerTypeEntity ent : cityEntityList) {
                CustomerType cityTo = new CustomerType();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;

        } catch (BeansException ex) {
            System.out.println("getCustomerTypeNameLike Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCustomerTypeNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     * @param tierId
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public CustomerType getCustTypeId(Long tierId) throws AppException {
        // TODO Auto-generated method stub
        try {
            CustomerTypeEntity cityEnt = customerTypeRepository.findByCustTypeId(tierId);
            CustomerType cityTo = new CustomerType();
            BeanUtils.copyProperties(cityEnt, cityTo);
            return cityTo;
        } catch (BeansException ex) {
            System.out.println("getCustomerTypeId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getCustomerTypeId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
    
   

}
