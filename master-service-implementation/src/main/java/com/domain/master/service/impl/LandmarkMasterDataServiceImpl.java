package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.LandmarkEntity;

import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.LandmarkMasterDataService;

import com.domain.master.masterservice.dto.Landmark;

import com.domain.master.repository.LandmarkRepository;
import org.springframework.beans.BeansException;

@Service("landmarkMasterDataService")
public class LandmarkMasterDataServiceImpl implements LandmarkMasterDataService {

    @Autowired
    LandmarkRepository landmarkRepository;

    /**
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<Landmark> getAllLandmark() throws AppException {
        try {
            List<Landmark> landmarkToList = new ArrayList<Landmark>();
            List<LandmarkEntity> landmarkEntityList = landmarkRepository.findAll();
            for (LandmarkEntity ent : landmarkEntityList) {
                Landmark landmarkTo = new Landmark();
                BeanUtils.copyProperties(ent, landmarkTo);
                landmarkToList.add(landmarkTo);
            }
            return landmarkToList;
        } catch (BeansException ex) {
            System.out.println("getAllCities Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllCities Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     * @param landmark
     * @throws com.domain.master.interfaces.AppException
     */
//    @Override
//    public Landmark getLandmarkName(String landmark) throws AppException {
//        try {
//            LandmarkEntity landmarkEnt = landmarkRepository.findByLandmarkName(landmark);
//            Landmark landmarkTo = new Landmark();
//            BeanUtils.copyProperties(landmarkEnt, landmarkTo);
//            return landmarkTo;
//        } catch (BeansException ex) {
//            System.out.println("getLandmarkName Bean Exception !!! " + ex.getMessage());
//            throw new AppException();
//        } catch (Exception ex) {
//            System.out.println("getLandmarkName Exception !!! " + ex.getMessage());
//            throw new AppException();
//        }
//    }
//
//    /**
//     * @param landmark
//     * @throws com.domain.master.interfaces.AppException
//     */
//    @Override
//    public List<Landmark> getLandmarkNameLike(String landmark) throws AppException {
//        try {
//            System.out.println("getLandmarkNameLike !!! " + landmark);
//            List<Landmark> landmarkToList = new ArrayList<Landmark>();
//            List<LandmarkEntity> landmarkEntityList;
//            landmarkEntityList = landmarkRepository.findByLandmarkNameContaining(landmark);
//            for (LandmarkEntity ent : landmarkEntityList) {
//                Landmark landmarkTo = new Landmark();
//                BeanUtils.copyProperties(ent, landmarkTo);
//                landmarkToList.add(landmarkTo);
//            }
//            return landmarkToList;
//
//        } catch (BeansException ex) {
//            System.out.println("getLandmarkNameLike Bean Exception !!! " + ex.getMessage());
//            throw new AppException();
//        } catch (Exception ex) {
//            System.out.println("getLandmarkNameLike Exception !!! " + ex.getMessage());
//            throw new AppException();
//        }
//    }
//
//    /**
//     * @param landmarkId
//     * @throws com.domain.master.interfaces.AppException
//     */
//    @Override
//    public Landmark getLandmarkId(Long landmarkId) throws AppException {
//        // TODO Auto-generated method stub
//        try {
//            LandmarkEntity landmarkEnt = landmarkRepository.findByLandmarkId(landmarkId);
//            Landmark landmarkTo = new Landmark();
//            BeanUtils.copyProperties(landmarkEnt, landmarkTo);
//            return landmarkTo;
//        } catch (BeansException ex) {
//            System.out.println("getLandmarkId Bean Exception !!! " + ex.getMessage());
//            throw new AppException();
//        } catch (Exception ex) {
//            System.out.println("getLandmarkId Exception !!! " + ex.getMessage());
//            throw new AppException();
//        }
//    }

    

}
