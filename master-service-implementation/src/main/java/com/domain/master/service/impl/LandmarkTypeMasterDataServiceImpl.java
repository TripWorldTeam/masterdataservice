package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.CountryEntity;
import com.domain.master.datamodel.CustomerEntity;
import com.domain.master.datamodel.LandmarkTypeEntity;
import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.LandmarkTypeMasterDataService;


import com.domain.master.masterservice.dto.Customers;
import com.domain.master.masterservice.dto.LandmarkType;


import com.domain.master.repository.CustomerRepository;
import com.domain.master.repository.LandmarkTypeRepository;
import org.springframework.beans.BeansException;

@Service("landmarkTypeMasterDataService")
public class LandmarkTypeMasterDataServiceImpl implements LandmarkTypeMasterDataService {


    @Autowired
    LandmarkTypeRepository landmarkTypeRepository;

    

    /**
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<LandmarkType> getAllLandmarkType() throws AppException {
        try {
            List<LandmarkType> cityToList = new ArrayList<LandmarkType>();
            List<LandmarkTypeEntity> cityEntityList = landmarkTypeRepository.findAll();
            for (LandmarkTypeEntity ent : cityEntityList) {
                LandmarkType cityTo = new LandmarkType();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;
        } catch (BeansException ex) {
            System.out.println("getAllLandmarkType Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllLandmarkType Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

   
    /**
     * @param city
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<LandmarkType> getLandmarkDescriptionLike(String cTypeDesc) throws AppException {
        try {
            System.out.println("getLandmarkDescriptionLike !!! " + cTypeDesc);
            List<LandmarkType> cityToList = new ArrayList<LandmarkType>();
            List<LandmarkTypeEntity> cityEntityList;
            cityEntityList = landmarkTypeRepository.findByLandmarkDescriptionContaining(cTypeDesc);
            for (LandmarkTypeEntity ent : cityEntityList) {
                LandmarkType cityTo = new LandmarkType();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;

        } catch (BeansException ex) {
            System.out.println("getLandmarkDescriptionLike Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getLandmarkDescriptionLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     * @param tierId
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public LandmarkType getLandmarkTypeId(String tierId) throws AppException {
        // TODO Auto-generated method stub
        try {
            LandmarkTypeEntity cityEnt = landmarkTypeRepository.findByLandmarkTypeId(tierId);
            LandmarkType cityTo = new LandmarkType();
            BeanUtils.copyProperties(cityEnt, cityTo);
            return cityTo;
        } catch (BeansException ex) {
            System.out.println("getLandmarkTypeId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getLandmarkTypeId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
    
   

}
