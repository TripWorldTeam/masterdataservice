package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.CountryEntity;

import com.domain.master.datamodel.AreaEntity;
import com.domain.master.datamodel.CustomerEntity;
import com.domain.master.datamodel.ContinentEntity;
import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.IMasterDataService;
import com.domain.master.masterservice.dto.CountryTO;

import com.domain.master.masterservice.dto.AreaM;
import com.domain.master.masterservice.dto.Customers;
import com.domain.master.masterservice.dto.Continent;

import com.domain.master.repository.CountryRepository;

import com.domain.master.repository.AreaRepository;
import com.domain.master.repository.CustomerRepository;
import com.domain.master.repository.ContinentRepository;

@Service("masterDataService")
public class MasterDataServiceImpl implements IMasterDataService {

//    private CityRepository cityRepository;
//    private CountryRepository countryRepository;
//     
//    @Autowired
//    public MasterDataServiceImpl(CityRepository cityRepository, CountryRepository countryRepository) {
//        this.cityRepository = cityRepository;
//        this.countryRepository = countryRepository;
//    }
    @Autowired
    CountryRepository countryRepository;

    @Autowired
    AreaRepository areaRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ContinentRepository continentRepository;

    @Override
    public List<CountryTO> getAllCountries() {
        // TODO Auto-generated method stub
        List<CountryTO> cntryToList = new ArrayList<CountryTO>();
        List<CountryEntity> cntryEntityList = countryRepository.findAll();
        for (CountryEntity ent : cntryEntityList) {
            CountryTO cntryTo = new CountryTO();
            BeanUtils.copyProperties(ent, cntryTo);
            cntryToList.add(cntryTo);
        }
        return cntryToList;
    }

    @Override
    public CountryTO getCountry(String countryCode) {
        // TODO Auto-generated method stub
        CountryEntity cntryEnt = countryRepository.findByCountryCode(countryCode);
        CountryTO cntryTo = new CountryTO();
        BeanUtils.copyProperties(cntryEnt, cntryTo);
        return cntryTo;
    }

   

   

}
