package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.PartnerEntity;

import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.PartnerMasterDataService;

import com.domain.master.masterservice.dto.Partner;

import com.domain.master.repository.PartnerRepository;
import org.springframework.beans.BeansException;

@Service("partnerMasterDataService")
public class PartnerMasterDataServiceImpl implements PartnerMasterDataService {

    @Autowired
    PartnerRepository partnerRepository;

    /**
     *
     * @return
     */
    /**
     * @return ********************************
     */
    @Override
    public List<Partner> getAllPartner() throws AppException {
        try {
            List<Partner> partnerToList = new ArrayList<Partner>();
            List<PartnerEntity> partnerEntityList = partnerRepository.findAll();
            for (PartnerEntity ent : partnerEntityList) {
                Partner partnerTo = new Partner();
                BeanUtils.copyProperties(ent, partnerTo);
                partnerToList.add(partnerTo);
            }
            return partnerToList;
        } catch (BeansException ex) {
            System.out.println("getAllPartner Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllPartner Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @Override
    public Partner getPartnerName(String city) throws AppException {
        try {
            PartnerEntity partnerEnt = partnerRepository.findByPartnerName(city);
            Partner partnerTo = new Partner();
            BeanUtils.copyProperties(partnerEnt, partnerTo);
            return partnerTo;
        } catch (BeansException ex) {
            System.out.println("getPartnerName Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getPartnerName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @Override
    public Partner getPartnerId(String partnerId) throws AppException {
        try {
            PartnerEntity partnerEnt = partnerRepository.findByPartnerId(partnerId);
            Partner partnerTo = new Partner();
            BeanUtils.copyProperties(partnerEnt, partnerTo);
            return partnerTo;
        } catch (BeansException ex) {
            System.out.println("getPartnerId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getPartnerId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
    @Override
    public List<Partner> getPartnerNameLike(String partnerName) throws AppException {
        try {
            System.out.println("getPartnerNameLike !!! " + partnerName);
            List<Partner> cityToList = new ArrayList<Partner>();
            List<PartnerEntity> cityEntityList;
            cityEntityList =  partnerRepository.findByPartnerNameContaining(partnerName);
            for (PartnerEntity ent : cityEntityList) {
                Partner cityTo = new Partner();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;

        } catch (BeansException ex) {
            System.out.println("getPartnerNameLike Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getPartnerNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }


}
