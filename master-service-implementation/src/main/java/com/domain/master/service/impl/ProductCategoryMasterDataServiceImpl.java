package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.ProductCategoryEntity;

import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.ProductCategoryMasterDataService;

import com.domain.master.masterservice.dto.ProductCategory;

import com.domain.master.repository.ProductCategoryRepository;
import org.springframework.beans.BeansException;

@Service("productCategoryMasterDataService")
public class ProductCategoryMasterDataServiceImpl implements ProductCategoryMasterDataService {

    @Autowired
    ProductCategoryRepository productRepository;

    /**
     *
     * @return
     */
    /**
     * @return ********************************
     */
    @Override
    public List<ProductCategory> getAllProductCategory() throws AppException {
        try {
            List<ProductCategory> productToList = new ArrayList<ProductCategory>();
            List<ProductCategoryEntity> productEntityList = productRepository.findAll();
            for (ProductCategoryEntity ent : productEntityList) {
                ProductCategory productTo = new ProductCategory();
                BeanUtils.copyProperties(ent, productTo);
                productToList.add(productTo);
            }
            return productToList;
        } catch (BeansException ex) {
            System.out.println("getAllProductCategory Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllProductCategory Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

   

    @Override
    public ProductCategory getProductCategoryId(String productId) throws AppException {
        try {
            ProductCategoryEntity productEnt = productRepository.findByProductCategoryId(productId);
            ProductCategory productTo = new ProductCategory();
            BeanUtils.copyProperties(productEnt, productTo);
            return productTo;
        } catch (BeansException ex) {
            System.out.println("getProductCategoryId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getProductCategoryId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
   


}
