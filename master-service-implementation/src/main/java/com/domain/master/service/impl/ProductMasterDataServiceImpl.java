package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.ProductEntity;

import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.ProductMasterDataService;

import com.domain.master.masterservice.dto.Product;

import com.domain.master.repository.ProductRepository;
import org.springframework.beans.BeansException;

@Service("productMasterDataService")
public class ProductMasterDataServiceImpl implements ProductMasterDataService {

    @Autowired
    ProductRepository productRepository;

    /**
     *
     * @return
     */
    /**
     * @return ********************************
     */
    @Override
    public List<Product> getAllProduct() throws AppException {
        try {
            List<Product> productToList = new ArrayList<Product>();
            List<ProductEntity> productEntityList = productRepository.findAll();
            for (ProductEntity ent : productEntityList) {
                Product productTo = new Product();
                BeanUtils.copyProperties(ent, productTo);
                productToList.add(productTo);
            }
            return productToList;
        } catch (BeansException ex) {
            System.out.println("getAllProduct Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllProduct Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @Override
    public Product getProductName(String city) throws AppException {
        try {
            ProductEntity productEnt = productRepository.findByProductName(city);
            Product productTo = new Product();
            BeanUtils.copyProperties(productEnt, productTo);
            return productTo;
        } catch (BeansException ex) {
            System.out.println("getProductName Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getProductName Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    @Override
    public Product getProductId(String productId) throws AppException {
        try {
            ProductEntity productEnt = productRepository.findByProductId(productId);
            Product productTo = new Product();
            BeanUtils.copyProperties(productEnt, productTo);
            return productTo;
        } catch (BeansException ex) {
            System.out.println("getProductId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getProductId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
    @Override
    public List<Product> getProductNameLike(String productName) throws AppException {
        try {
            System.out.println("getProductNameLike !!! " + productName);
            List<Product> cityToList = new ArrayList<Product>();
            List<ProductEntity> cityEntityList;
            cityEntityList =  productRepository.findByProductNameContaining(productName);
            for (ProductEntity ent : cityEntityList) {
                Product cityTo = new Product();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;

        } catch (BeansException ex) {
            System.out.println("getProductNameLike Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getProductNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
    
    @Override
    public List<Product> getPartnerId(String partnerId) throws AppException {
        try {
            System.out.println("getProductNameLike !!! " + partnerId);
            List<Product> cityToList = new ArrayList<Product>();
            List<ProductEntity> cityEntityList;
            cityEntityList =  productRepository.findByPartnerId(partnerId);
            for (ProductEntity ent : cityEntityList) {
                Product cityTo = new Product();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;

        } catch (BeansException ex) {
            System.out.println("getProductNameLike Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getProductNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }


}
