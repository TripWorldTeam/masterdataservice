package com.domain.master.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.master.datamodel.CountryEntity;
import com.domain.master.datamodel.ProductEntity;
import com.domain.master.datamodel.ProductTypeEntity;
import com.domain.master.interfaces.AppException;

import com.domain.master.interfaces.ProductTypeMasterDataService;


import com.domain.master.masterservice.dto.ProductType;


import com.domain.master.repository.ProductRepository;
import com.domain.master.repository.ProductTypeRepository;
import org.springframework.beans.BeansException;

@Service("productTypeMasterDataService")
public class ProductTypeMasterDataServiceImpl implements ProductTypeMasterDataService {


    @Autowired
    ProductTypeRepository productTypeRepository;

    @Autowired
    ProductRepository productRepository;


    /**
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<ProductType> getAllProductType() throws AppException {
        try {
            List<ProductType> cityToList = new ArrayList<ProductType>();
            List<ProductTypeEntity> cityEntityList = productTypeRepository.findAll();
            for (ProductTypeEntity ent : cityEntityList) {
                ProductType cityTo = new ProductType();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;
        } catch (BeansException ex) {
            System.out.println("getAllProductType Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getAllProductType Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

   
    /**
     * @param city
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public List<ProductType> getProductTypeDescriptionLike(String cTypeDesc) throws AppException {
        try {
            System.out.println("getProductTypeNameLike !!! " + cTypeDesc);
            List<ProductType> cityToList = new ArrayList<ProductType>();
            List<ProductTypeEntity> cityEntityList;
            cityEntityList = productTypeRepository.findByProductTypeDescriptionContaining(cTypeDesc);
            for (ProductTypeEntity ent : cityEntityList) {
                ProductType cityTo = new ProductType();
                BeanUtils.copyProperties(ent, cityTo);
                cityToList.add(cityTo);
            }
            return cityToList;

        } catch (BeansException ex) {
            System.out.println("getProductTypeNameLike Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getProductTypeNameLike Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }

    /**
     * @param tierId
     * @throws com.domain.master.interfaces.AppException
     */
    @Override
    public ProductType getProductTypeId(Long tierId) throws AppException {
        // TODO Auto-generated method stub
        try {
            ProductTypeEntity cityEnt = productTypeRepository.findByProductTypeId(tierId);
            ProductType cityTo = new ProductType();
            BeanUtils.copyProperties(cityEnt, cityTo);
            return cityTo;
        } catch (BeansException ex) {
            System.out.println("getProductTypeId Bean Exception !!! " + ex.getMessage());
            throw new AppException();
        } catch (Exception ex) {
            System.out.println("getProductTypeId Exception !!! " + ex.getMessage());
            throw new AppException();
        }
    }
    
   

}
