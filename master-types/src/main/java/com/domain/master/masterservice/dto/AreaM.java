/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author root1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AreaM extends BaseTransferObject {

    /*setting up the Crs_cities tables fields.*/
    private static final long serialVersionUID = 1L;

    private String areaId;
    private String areaName;
    private String cityCode;

    /**
     * Setting up the Getter and Setter for the All the CRS Cites table.
     */
    /*get the Id*/
    public String getAreaId() {
        return areaId;
    }

    /*Set the Id*/
    public void setAreaId(String id) {
        this.areaId = id;
    }

    

    /*get the areaName*/
    public String getAreaName() {
        return areaName;
    }

    /*Set the areaName*/
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /*get the CityCode*/
    public String getCityCode() {
        return cityCode;
    }

    /*Set the CityCode*/
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

   
}
