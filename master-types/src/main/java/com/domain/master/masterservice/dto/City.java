/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author root1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class City extends BaseTransferObject {

    /*setting up the Crs_cities tables fields.*/
    private static final long serialVersionUID = 1L;

    private Long cityId;
    private Long countryId;
    private String city;
    private String countryName;
    private String cityName;

    /**
     * Setting up the Getter and Setter for the All the CRS Cites table.
     */
    /*get the Id*/
    public Long getId() {
        return cityId;
    }

    /*Set the Id*/
    public void setId(Long id) {
        this.cityId = id;
    }

    /*get the CountryId*/
    public Long getCountryId() {
        return countryId;
    }

    /*Set the CountryId*/
    public void setCountryId(Long cid) {
        this.countryId = cid;
    }

    /*get the City*/
    public String getCity() {
        return city;
    }

    /*Set the City*/
    public void setCity(String city) {
        this.city = city;
    }

    /*get the CountryName*/
    public String getCountryName() {
        return countryName;
    }

    /*Set the CountryName*/
    public void setCountryName(String country) {
        this.countryName = country;
    }

    /*get the CityName*/
    public String getCityName() {
        return cityName;
    }

    /*Set the CityName*/
    public void setCityName(String city) {
        this.cityName = city;
    }
}
