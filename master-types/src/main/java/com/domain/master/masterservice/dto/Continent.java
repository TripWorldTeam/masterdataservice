/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;

/**
 *
 * @author root1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Continent extends BaseTransferObject {

    /*setting up the Crs_cities tables fields.*/
    private static final long serialVersionUID = 1L;

    private Integer continentId;
    private String continentName;
    private String countryCode;

    /*get the Customer Id*/
    public Integer getId() {
        return continentId;
    }

    /*get the Customer Id*/
    public void setId(Integer id) {
        this.continentId = id;
    }
    public Integer getContinentId() {
        return continentId;
    }

    /*get the Customer Id*/
    public void setContinentId(Integer id) {
        this.continentId = id;
    }

    /*get the Customer Name*/
    public String getcontinentName() {
        return continentName;
    }

    /*set the Customer Name */
    public void setcontinentName(String cusName) {
        this.continentName = cusName;
    }

    /*get the Complete City Address*/
    public String getCountryCode() {
        return countryCode;
    }

    /*set the Complete City Address*/
    public void setCountryCode(String country_code) {
        this.countryCode = country_code;
    }

}
