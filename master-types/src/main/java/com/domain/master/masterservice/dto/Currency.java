/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author root1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Currency extends BaseTransferObject {

    /*setting up the Crs_cities tables fields.*/
    private static final long serialVersionUID = 1L;

    private String currencyCode;
    private String currencyDescription;
    

   
    /*get the Currency Id*/
    public String getId() {
        return currencyCode;
    }

    /*get the CurrencyCode Id*/
    public void setId(String id) {
        this.currencyCode = id;
    }

    /*get the Currency Id*/
    public String getCurrencyCode() {
        return currencyCode;
    }

    /*set the Currency Id*/
    public void setCurrencyCode(String currCode) {
        this.currencyCode = currCode;
    }

    /*get the CurrencyDescription*/
    public String getCurrencyDescription() {
        return currencyDescription;
    }

    /*set the CurrencyDescription*/
    public void setCurrencyDescription(String description) {
        this.currencyDescription = description;
    }
   
}
