package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerTier extends BaseTransferObject {

    private static final long serialVersionUID = 1L;

    private Long customerTierId;
    private String customerTierName;
    private String customerId;

    public Long getId() {
        return customerTierId;
    }

    public void setId(Long id) {
        this.customerTierId = id;
    }

    public String getCustomerTierName() {
        return customerTierName;
    }

    public void setCustomerTierName(String countryName) {
        this.customerTierName = countryName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Long getCustomerTierId() {
        return customerTierId;
    }

    public void setCustomerTierId(Long customerTierId) {
        this.customerTierId = customerTierId;
    }

}
