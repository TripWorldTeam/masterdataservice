package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerType extends BaseTransferObject {

    private static final long serialVersionUID = 1L;

    private Long custTypeId;
    private String custTypeDesc;

    

    public Long getId() {
        return custTypeId;
    }

    public void setId(Long id) {
        this.custTypeId = id;
    }

    public String getCustTypeDesc() {
        return custTypeDesc;
    }

    public void setCustTypeDesc(String typeDesc) {
        this.custTypeDesc = typeDesc;
    }

    public Long getCustTypeId() {
        return custTypeId;
    }

    public void setCustTypeId(Long customerTypeId) {
        this.custTypeId = customerTypeId;
    }

}
