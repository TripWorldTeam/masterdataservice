/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author root1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Landmark extends BaseTransferObject {

    /*setting up the Crs_cities tables fields.*/
    private static final long serialVersionUID = 1L;

    private String landmarkId;
    private String landmarkDescription;
    private String landmarkTypeId;
    private String areaId;
    private Float latitude;
    private Float longtitude;

    /*get the Landmark Id*/
    public String getId() {
        return landmarkId;
    }

    /*get the Landmark Id*/
    public void setId(String id) {
        this.landmarkId = id;
    }

    /*get the Country Id*/
    public String getLandmarkDescription() {
        return landmarkDescription;
    }

    /*set the Country Id*/
    public void setLandmarkDescription(String landDescription) {
        this.landmarkDescription = landDescription;
    }

    /*get the landmarkTypeId*/
    public String getLandmarkTypeId() {
        return landmarkTypeId;
    }

    /*set the landmarkTypeId*/
    public void setLandmarkTypeId(String landmarkTypeId) {
        this.landmarkTypeId = landmarkTypeId;
    }

    /*get the Area Id*/
    public String getAreaId() {
        return areaId;
    }

    /*set the Area Name*/
    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    /*get the latitude */
    public Float getLatitude() {
        return latitude;
    }

    /*set the latitude*/
    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLandmarkId() {
        return longtitude;
    }

    public void setLandmarkId(Float longtitude) {
        this.longtitude = longtitude;
    }

}
