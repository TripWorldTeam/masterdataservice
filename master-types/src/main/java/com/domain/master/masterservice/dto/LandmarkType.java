package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LandmarkType extends BaseTransferObject {

    private static final long serialVersionUID = 1L;

    private String landmarkTypeId;
    private String landmarkDescription;

    

    public String getId() {
        return landmarkTypeId;
    }

    public void setId(String id) {
        this.landmarkTypeId = id;
    }

    public String getCustTypeDesc() {
        return landmarkDescription;
    }

    public void setCustTypeDesc(String typeDesc) {
        this.landmarkDescription = typeDesc;
    }

    public String getLandmarkTypeId() {
        return landmarkTypeId;
    }

    public void setLandmarkTypeId(String landmarkTypeId) {
        this.landmarkTypeId = landmarkTypeId;
    }

}
