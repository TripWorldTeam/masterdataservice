/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;

/**
 *
 * @author root1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Partner extends BaseTransferObject {

    /*setting up the Crs_cities tables fields.*/
    private static final long serialVersionUID = 1L;

    private String partnerId;
    private String partnerName;
    private String partnerAddress;
    private String partnerCityCode;
    private String partnerCountryCode;
    private String partnerLocation;
    private Double partner_Hq_Address;
    private String partnerTypeId;
    private Integer partnerValue;
    private String partnerStatus;
    private Date partnerOnBoardingDate;
    private Date partnerTerminationDate;
    private String partnerTerminationReason;
    
    
    /*get the Partner Id*/
    public String getId() {
        return partnerId;
    }

    /*get the Partner Id*/
    public void setId(String id) {
        this.partnerId = id;
    }
    public String getPartnerId() {
        return partnerId;
    }

    /*get the Partner Id*/
    public void setPartnerId(String id) {
        this.partnerId = id;
    }

    /*get the Partner Name*/
    public String getPartnerName() {
        return partnerName;
    }

    /*set the Partner Name */
    public void setPartnerName(String cusName) {
        this.partnerName = cusName;
    }

    /*get the Complete City Address*/
    public String getPartnerAddress() {
        return partnerAddress;
    }

    /*set the Complete City Address*/
    public void setPartnerAddress(String address) {
        this.partnerAddress = address;
    }

    /*get the PartnerCityCode*/
    public String getPartnerCityCode() {
        return partnerCityCode;
    }

    /*set the Partner Contact Email*/
    public void setPartnerCityCode(String partnerCityCode) {
        this.partnerCityCode = partnerCityCode;
    }

    /*get the Partner Contact Phone*/
    public String getPartnerCountryCode() {
        return partnerCountryCode;
    }

    /*set the Partner Contact Phone*/
    public void setPartnerCountryCode(String partnerCountryCode) {
        this.partnerCountryCode = partnerCountryCode;
    }

    
    /*get the Partner Location*/
    public String getPartnerLocation() {
        return partnerLocation;
    }

    /*set the Partner Location*/
    public void setPartnerLocation(String partnerLocation) {
        this.partnerLocation = partnerLocation;
    }
    
    /*get the Partner Location Latitude*/
    public Double getPartnerHqAddress() {
        return partner_Hq_Address;
    }

    /*set the Partner Location Latitude*/
    public void setPartnerHqAddress(Double partner_hq_address) {
        this.partner_Hq_Address = partner_hq_address;
    }

    /*get the Partner Type ID*/
    public String getPartnerTypeId() {
        return partnerTypeId;
    }

    /*set the Partner Type ID*/
    public void setPartnerTypeId(String partnerTypeId) {
        this.partnerTypeId = partnerTypeId;
    }

    /*get the Partner Contact Social*/
    public Integer getPartnerValue() {
        return partnerValue;
    }

    /*set the Partner Linked Partners*/
    public void setPartnerValue(Integer partnerValue) {
        this.partnerValue = partnerValue;
    }
    
    /*get the Partner Contact Social*/
    public String getPartnerStatus() {
        return partnerStatus;
    }

    /*set the Partner Linked Partners*/
    public void setPartnerStatus(String cus_status) {
        this.partnerStatus = cus_status;
    }
    
    /*get the Partner Contact Social*/
    public Date getPartnerOnBoardingDate() {
        return partnerOnBoardingDate;
    }

    /*set the Partner Linked Partners*/
    public void setPartnerOnBoardingDate(Date partnerOnBoardingDate) {
        this.partnerOnBoardingDate = partnerOnBoardingDate;
    }
    /*get the Partner Contact Social*/
    public Date getPartnerTerminationDate() {
        return partnerTerminationDate;
    }

    /*set the Partner Linked Partners*/
    public void setPartnerTerminationDate(Date partnerTerminationDate) {
        this.partnerTerminationDate = partnerTerminationDate;
    }
    
    /*get the Partner Contact Social*/
    public String getPartnerTerminationReason() {
        return partnerTerminationReason;
    }

    /*set the Partner Linked Partners*/
    public void setPartnerTerminationReason(String partnerTerminationReason) {
        this.partnerTerminationReason = partnerTerminationReason;
    }
}
