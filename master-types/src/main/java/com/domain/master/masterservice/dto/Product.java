/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;


/**
 *
 * @author root1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product extends BaseTransferObject {

    /*setting up the Crs_cities tables fields.*/
    private static final long serialVersionUID = 1L;

    private String productId;
    private String productName;
    private String productDescription;
    
    private String partnerId;
//    private Partner partnerId;
    private Integer productTypeId;
    private String productSpecification;
    private String productCategoryId;
    private Integer productAmenitiesGroup;
    private String productImage;
    private Integer productQty;
    
   
    
    /*get the Product Id*/
    public String getId() {
        return productId;
    }

    /*get the Product Id*/
    public void setId(String id) {
        this.productId = id;
    }
    public String getProductId() {
        return productId;
    }

    /*get the Product Id*/
    public void setProductId(String id) {
        this.productId = id;
    }

    /*get the Product Name*/
    public String getProductName() {
        return productName;
    }

    /*set the Product Name */
    public void setProductName(String cusName) {
        this.productName = cusName;
    }

    /*get the Complete City Address*/
    public String getProductDescription() {
        return productDescription;
    }

    /*set the Complete City Address*/
    public void setProductDescription(String desciption) {
        this.productDescription = desciption;
    }

    /*get the ProductCityCode*/
    public String getpartnerId() {
        return partnerId;
    }

    /*set the Product Contact Email*/
    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    /*get the Product Contact Phone*/
    public Integer getProductTypeId() {
        return productTypeId;
    }

    /*set the Product Contact Phone*/
    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    
    /*get the Product Location*/
    public String getProductSpecification() {
        return productSpecification;
    }

    /*set the Product Location*/
    public void setProductSpecification(String productSpecification) {
        this.productSpecification = productSpecification;
    }
    
    /*get the Product Location Latitude*/
    public String getProductCategoryId() {
        return productCategoryId;
    }

    /*set the Product Location Latitude*/
    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

   

    /*get the Product Contact Social*/
    public Integer getProductAmenitiesGroup() {
        return productAmenitiesGroup;
    }

    /*set the Product Linked Products*/
    public void setProductAmenitiesGroup(Integer productAmenitiesGroup) {
        this.productAmenitiesGroup = productAmenitiesGroup;
    }
    
    /*get the Product Contact Social*/
    public String getProductImage() {
        return productImage;
    }

    /*set the Product Linked Products*/
    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }
    
    
    /*get the Product Contact Social*/
    public Integer getProductQty() {
        return productQty;
    }

    /*set the Product Linked Products*/
    public void setProductQty(int productQty) {
        this.productQty = productQty;
    }
    
}
