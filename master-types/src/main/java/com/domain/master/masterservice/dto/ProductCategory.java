/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;


/**
 *
 * @author root1
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductCategory extends BaseTransferObject {

    /*setting up the Crs_cities tables fields.*/
    private static final long serialVersionUID = 1L;

    private String productCategoryId;
    private String productCategoryDescription;
    private Date productCategoryStartDate;
    private String productCategoryStatus;
    private String productId;
    
   
    
    /*get the Product Id*/
    public String getId() {
        return productCategoryId;
    }

    /*get the Product Id*/
    public void setId(String id) {
        this.productCategoryId = id;
    }
    
    public String getProductCategoryId() {
        return productCategoryId;
    }

    /*get the Product Id*/
    public void setProductCategoryId(String id) {
        this.productCategoryId = id;
    }
    
    public String getProductId() {
        return productId;
    }

    /*get the Product Id*/
    public void setProductId(String id) {
        this.productId = id;
    }

    /*get the Product Name*/
    public String getProductCategoryDescription() {
        return productCategoryDescription;
    }

    /*set the Product Name */
    public void setProductCategoryDescription(String productName) {
        this.productCategoryDescription = productName;
    }

    /*get the Complete City Address*/
    public Date getProductCategoryStartDate() {
        return productCategoryStartDate;
    }

    /*set the Complete City Address*/
    public void setProductCategoryStartDate(Date date) {
        this.productCategoryStartDate = date;
    }

    /*get the ProductCityCode*/
    public String getProductCategoryStatus() {
        return productCategoryStatus;
    }

    /*set the Product Contact Email*/
    public void setProductCategoryStatus(String productCategoryStatus) {
        this.productCategoryStatus = productCategoryStatus;
    }

    
}
