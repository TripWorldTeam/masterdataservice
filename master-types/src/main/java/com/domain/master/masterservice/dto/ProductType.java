package com.domain.master.masterservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductType extends BaseTransferObject {

    private static final long serialVersionUID = 1L;

    private Long productTypeId;
    private String productTypeDescription;
    private Date startDate;
    private String productTypeStatus;

    

    public Long getId() {
        return productTypeId;
    }

    public void setId(Long id) {
        this.productTypeId = id;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }
    
    public String getProductTypeDescription() {
        return productTypeDescription;
    }

    public void setProductTypeDescription(String prodDesc) {
        this.productTypeDescription = prodDesc;
    }
    
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public String getproductTypeStatus() {
        return productTypeStatus;
    }

    public void setproductTypeStatus(String proStatus) {
        this.productTypeStatus = proStatus;
    }
}
